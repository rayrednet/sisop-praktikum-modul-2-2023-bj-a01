# Praktikum Sistem Operasi Modul 2 - Proses dan Daemon
## _Kelompok A01_
---
#### Anggota Kelompok

| Nama                                 | NRP        |
| -------------------------------------|------------|
| Gabrielle Immanuel Osvaldo Kurniawan | 5025211135 |
| Rr. Diajeng Alfisyahrinnisa Anandha  | 5025211147 |
| Rayssa Ravelia                       | 5025211219 |


## 🐈 Nomor 1 
### Soal
Grape-kun adalah seorang penjaga hewan di kebun binatang, dia mendapatkan tugas dari atasannya untuk melakukan penjagaan pada beberapa hewan-hewan yang ada di kebun binatang sebelum melakukan penjagaan Grape-kun harus mengetahui terlebih dahulu hewan apa aja yang harus dijaga dalam drive kebun binatang tersebut terdapat folder gambar dari hewan apa saja yang harus dijaga oleh Grape-kun. Berikut merupakan link download dari drive kebun binatang tersebut : https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq 
- Grape-kun harus mendownload file tersebut untuk disimpan pada penyimpanan local komputernya. Dan untuk melakukan melihat file gambar pada folder yang telah didownload Grape-kun harus melakukan unzip pada folder tersebut.
- Setelah berhasil melakukan unzip Grape-kun melakukan pemilihan secara acak pada file gambar tersebut untuk melakukan shift penjagaan pada hewan tersebut.
- Karena Grape-kun adalah orang yang perfeksionis Grape-kun ingin membuat direktori untuk memilah file gambar tersebut. Direktori tersebut dengan nama HewanDarat, HewanAmphibi, dan HewanAir. Setelah membuat direktori tersebut Grape-kun harus melakukan filter atau pemindahan file gambar hewan sesuai dengan tempat tinggal nya.
- Setelah mengetahui hewan apa saja yang harus dijaga Grape-kun melakukan zip kepada direktori yang dia buat sebelumnya agar menghemat penyimpanan.
Catatan : 
untuk melakukan zip dan unzip tidak boleh menggunakan system

### Jawab
Inti dari soal tersebut adalah kita harus membantu Grape-kun seorang penjaga kebun binatang. Grapekun harus download folder gambar dari hewan-hewan yang dia jaga, unzip folder, memilih gambar secara acak untuk menentukan jadwal shift, membuat direktori untuk foto berdasarkan habitat hewan-hewan, melakukan filter dan pindahkan foto-foto hewan ke direktori yang tepat, dan zip direktori untuk menghemat tempat penyimpanan. Pada soal ini kita tidak boleh menggunakan system() untuk zip dan unzip.

#### Alur Penyelesaian
Untuk menyelesaikan soal tersebut berikut ini adalah alur tahapan yang kami lakukan untuk mengerjakan soal tersebut:
1. Dowload zip dari link soal
2. Unzip file yang di-download
3. Memilih file random
4. Membuat direktori HewanDarat, HewanAmphibi, dan HewanAir
5. Filter file ke direktori yang sesuai
6. Zip ketiga direktori tersebut

#### 1. Download zip dari link soal
Berdasarkan perintah soal, kami tidak diperbolehkan menggunakan fungsi system() untuk melakukan zip dan unzip. Kita dapat menggunakan fork  untuk melakukan ```execlp``` yang bertujuan untuk melakukan proses download. Berikut ini adalah alur yang kami buat untuk melakukan download zip dari link drive:
1. Melakukan proses fork untuk membuat proses child
2. Cek jika fork berhasil atau tidak, cetak pesan error jika error dan keluar
3. Jika fork berhasil, lakukan eksekusi perintah ```wget``` untuk mendownload file zip. Jika eksekusi gagal, keluarkan pesan error
4. Jika fork berhasil dan prosesnya adalah parent process, tunggu proses child untuk selesai
5. Jika proses child berhasil, cetak pesan yang menunjukkan bahwa download berhasil

Dari alur tersebut, dapat dibuat script dengan bahasa pemrograman C untuk **download zip** sebagai berikut:
```c
// fork 1 - download zip from link
    pid = fork();
    if (pid < 0) {
        perror("fork 1 failed");
        exit(1);
    }
    else if (pid == 0) {
        // child process
        execlp("wget", "wget", "-O", "binatang.zip", "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", NULL);
        perror("exec failed");
        exit(1);
    }
    else {
    // parent process
    wait(&status);
    if (WIFEXITED(status) && !WEXITSTATUS(status)) {
        printf("Download completed successfully\n");
    }
```
##### Penjelasan code download zip lebih detail
Berdasarkan kode di atas, berikut ini adalah penjelasan kode secara detail:
```c
pid = fork();
```

>  Kode tersebut untuk melakukan **forking** atau proses pembuatan proses baru dari proses yang sedang berjalan. Setelah melakukan `fork()`, program akan **membagi dua proses** yang berjalan secara simultan, yaitu parent process dan child process. Pada **parent** process, pid akan menyimpan process ID dari child process, sedangkan pada **child** process, nilai pid akan menjadi **0**.

```c
if (pid < 0) {
        perror("fork 1 failed");
        exit(1);
    }
```
> Jika nilai pid yang dikembalikan oleh `fork()` adalah **kurang dari 0**, maka artinya proses forking **gagal** dilakukan. Hal ini dapat terjadi jika sistem tidak dapat membuat child process baru karena terdapat **batasan jumlah proses** yang sedang berjalan atau terjadi kesalahan sistem lainnya.

> Pada kode tersebut, `perror("fork 1 failed");` akan mencetak pesan error "fork 1 failed". Kemudian, `exit(1);` akan **menghentikan program secara paksa** dan mengembalikan **nilai 1**, yang menunjukkan bahwa terdapat kesalahan yang terjadi pada program.


```c
 else if (pid == 0) {
        // child process
        execlp("wget", "wget", "-O", "binatang.zip", "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", NULL);
        perror("exec failed");
        exit(1);
    }
```
> Jika proses forking berhasil, maka akan dilakukan pengecekan apakah proses yang berjalan saat ini adalah child process atau parent process dengan menggunakan perintah `if (pid == 0)`. Karena nilai pid pada **child process** selalu sama dengan **0**, maka kondisi tersebut hanya akan terpenuhi pada child process.

> Selanjutnya, pada bagian 
`execlp("wget", "wget", "-O", "binatang.zip", "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", NULL);`
program akan menjalankan perintah `wget` yang digunakan untuk **mendownload file dari internet**. Argumen `wget` pertama merupakan nama perintah yang akan dijalankan. Kemudian, argumen kedua dan seterusnya adalah argumen tambahan yang diperlukan oleh program tersebut. Pada kode ini, argumen `-O` digunakan untuk **menentukan output file** yang akan dihasilkan dari download. Sedangkan, argumen `binatang.zip` menentukan **nama file yang akan dihasilkan**. Kemudian, argumen terakhir adalah **URL tempat file** tersebut diunduh.

> Jika **child process** berhasil menjalankan perintah tersebut, maka program akan mencetak pesan "Download completed successfully". Namun, jika proses gagal, maka akan dihasilkan pesan error "exec failed". Setelah itu, child process akan **exit** dari program dengan **nilai 1**.

```c
else {
    // parent process
    wait(&status);
    if (WIFEXITED(status) && !WEXITSTATUS(status)) {
        printf("Download completed successfully\n");
    }
```
> Jika nilai **pid** yang dikembalikan oleh `fork()` adalah **lebih besar dari 0**, maka artinya proses forking **berhasil** dilakukan dan program berada pada parent process.

> Pada kode tersebut, `wait(&status);` digunakan untuk **menunggu child process** selesai dijalankan dan mengambil `status` dari proses tersebut. `status` pada kode tersebut adalah variable yang digunakan untuk **menyimpan status** dari proses child process.

> Selanjutnya, `if (WIFEXITED(status) && !WEXITSTATUS(status))` digunakan untuk **memeriksa** apakah proses child process selesai dengan **berhasil atau tidak**. Fungsi `WIFEXITED` digunakan untuk **memeriksa** apakah proses **child process** selesai dengan normal atau tidak, sedangkan` WEXITSTATUS` digunakan untuk **memeriksa nilai** yang dikembalikan oleh proses tersebut. Dalam hal ini, `!WEXITSTATUS(status)` akan bernilai `true` jika proses child process selesai **tanpa terjadi error**.

> Jika proses child process selesai dengan **berhasil**, maka program akan mencetak pesan "Download completed successfully". Namun, jika terjadi kesalahan pada proses child process, program akan berhenti secara normal.

#### 2. Unzip file yang di-download
Setelah melakukan download file zip tersebut, kita harus melakukan unzip tanpa menggunakan fungsi system().  Berikut ini adalah alur yang kami buat untuk melakukan unzip file dari link drive:
1. Lakukan forking untuk membuat child process baru
2. Cek forking berhasil atau tidak. Jika proses forking gagal, maka program akan menghentikan proses secara paksa dan mencetak pesan error
3. Jika forking berhasil, maka program berada pada child process. Pada tahap ini program menjalankan perintah untuk melakukan unzip file. Jika unzip tidak berhasil, keluarkan pesan error
4. Pada parent process, tunggu proses child selesai
5. Hapus file zip yang telah di-download dan cetak pesan yang menandakan unzip selesai. File yang di download tadi dihapus untuk menghemat tempat.

Dari alur tersebut, dapat dibuat script dengan bahasa pemrograman C untuk **unzip file** sebagai berikut:
```c
// fork 2 - unzip the file 
        pid_t pid2 = fork();
        if (pid2 < 0) {
            perror("fork 2 failed");
            exit(1);
        } else if (pid2 == 0) {
            execlp("unzip", "unzip", "binatang.zip", "-d", "binatang_folder", NULL);
            perror("exec failed");
            exit(1);
        } else {
            wait(&status);
            if (WIFEXITED(status) && !WEXITSTATUS(status)) {
                system("rm -rf binatang.zip");
                printf("Unzip completed successfully\n");
            }
```
##### Penjelasan code unzip file lebih detail
Berdasarkan kode di atas, berikut ini adalah penjelasan kode secara detail:
```c
pid_t pid2 = fork();
```
> Variabel `pid2` bertipe data `pid_t` digunakan untuk **menyimpan nilai process ID** dari proses yang telah di-fork. Jika proses forking berhasil, maka nilai `pid2` pada **parent process** akan menyimpan process ID dari child process yang baru dibuat. Sedangkan pada **child process**, nilai pid2 akan bernilai **0** karena tidak ada child process baru yang di-fork dari child process tersebut.

```c
if (pid2 < 0) {
            perror("fork 2 failed");
            exit(1);
}
```
> Kode tersebut melakukan _error handling_. Jika nilai dari `pid2` **kurang dari 0**, maka proses forking dianggap **gagal** dan program akan mengeksekusi blok kode di dalam if statement.

> Pada blok kode tersebut, `perror()` akan mencetak pesan error ke konsol "fork 2 failed". Selanjutnya, `exit(1)` digunakan untuk **menghentikan program** secara paksa dengan mengembalikan **nilai 1** sebagai kode kesalahan yang menandakan bahwa program berhenti karena kesalahan pada proses forking.

```c
 else if (pid2 == 0) {
            execlp("unzip", "unzip", "binatang.zip", "-d", "binatang_folder", NULL);
            perror("exec failed");
            exit(1);
        }
```
> Kode ini berfungsi untuk menge-unzip file yang telah di-download sebelumnya

> Dalam kode tersebut, `execlp()` digunakan untuk menjalankan perintah unzip di dalam **child process**. Fungsi `execlp()` merupakan salah satu dari sekian banyak fungsi yang disediakan oleh sistem operasi UNIX untuk **mengeksekusi perintah** atau program lain. Fungsi ini **menggantikan proses child process** yang sedang berjalan dengan program baru yang dijalankan oleh fungsi tersebut.

> Pada kode `execlp("unzip", "unzip", "binatang.zip", "-d", "binatang_folder", NULL)` berarti menjalankan **perintah unzip** dengan memberikan argumen` -d binatang_folder` untuk mengekstrak file `binatang.zip` ke dalam folder `binatang_folder`. 

> `unzip`: argumen ini digunakan sebagai nilai untuk **argv[0]** pada program yang akan dijalankan. **argv[0]** biasanya digunakan sebagai **identitas program** yang sedang dijalankan. Dalam hal ini, nilai unzip digunakan sebagai identitas program yang sedang dijalankan.

> `binatang.zip`: argumen ini merupakan **file yang akan di-unzip** oleh program unzip.

> `-d`: argumen ini digunakan untuk memberikan opsi pada program unzip agar file yang di-unzip akan **diletakkan ke dalam direktori** yang ditentukan selanjutnya.

> `binatang_folder`: argumen ini merupakan **direktori tempat file yang di-unzip** akan diletakkan.

> `NULL`: argumen ini menandakan **akhir dari daftar argumen** yang diberikan pada fungsi `execlp()`. Argumen ini selalu harus diberikan pada fungsi `execlp()` karena fungsi ini memerlukan informasi akhir dari daftar argumen yang diberikan.

> Jika `execlp()` gagal, maka blok kode berikutnya akan dijalankan. `perror("exec failed");` akan mencetak pesan error ke konsol"exec failed". Selanjutnya, `exit(1)` digunakan untuk **menghentikan child process** secara paksa dengan mengembalikan **nilai 1** sebagai kode kesalahan yang menandakan bahwa proses tersebut berhenti karena kesalahan dalam mengeksekusi perintah unzip.

```c
else {
            wait(&status);
            if (WIFEXITED(status) && !WEXITSTATUS(status)) {
                system("rm -rf binatang.zip");
                printf("Unzip completed successfully\n");
            }
```
> Setelah proses unzip selesai dijalankan di child process, maka **parent process** akan menunggu proses tersebut selesai dengan menggunakan fungsi `wait()`. Setelah proses unzip selesai dan berhasil dieksekusi, maka akan dieksekusi blok kode dalam **if-statement**

> Fungsi `WIFEXITED()` akan mengevaluasi apakah proses child **telah berhenti secara normal atau tidak**. Fungsi `WEXITSTATUS()` akan mengevaluasi apakah proses child telah berhenti dengan **kode keluaran yang berhasil atau tidak**. Jika proses child telah berhenti dengan kode keluaran yang berhasil, maka blok kode dalam if-statement akan dieksekusi

> Blok kode dalam **if-statement** akan **menghapus** file `binatang.zip` menggunakan perintah `system("rm -rf binatang.zip")`, yang mana perintah ini akan dijalankan di shell. Kemudian, pesan "Unzip completed successfully" akan dicetak ke layar menggunakan fungsi `printf()`.

#### 3. Memilih file random
Langkah selanjutnya setelah melakukan unzip file adalah kita harus memilih file random untuk Grape-kun melakukan shift penjagaan. Untuk memilih file random berikut ini adalah alur pengerjaan yang kami buat:
1. Inisialisasi seed fungsi random menggunakan waktu saat ini
2. Buka direktori `binatang_folder`
3. Hitung jumlah file dalam direktori tersebut agar angka random tidak melebihi jumlah file
4. Pada setiap file yang ditemukan, hitung probabilas untuk memilih file tersebut
5. Jika nilai acak yang dihasilkan kurang dari probabilitas, maka simpan nama file tersebut dalam buffer
6. Setelah semua file di dalam direktori telah diproses, tutup direktori
7. Cetak nama file yang terpilih secara random

Dari alur tersebut, dapat dibuat script dengan bahasa pemrograman C untuk **memilih random file** sebagai berikut:
```c
// choose random file
                srand(time(NULL));
                DIR *dir = opendir("binatang_folder");
                struct dirent *entry;
                int file_count = 0;
                while ((entry = readdir(dir)) != NULL) {
                    if (entry->d_type == DT_REG) {
                        file_count++;
                        if (rand() % file_count == 0) {
                            sprintf(random_file_buf, "%s", entry->d_name);
                        }
                    }
                }
                closedir(dir);
                printf("\033[1;36m\nFile gambar acak adalah: %s\033[0m\n\n", random_file_buf);  //adding cyan color :)
```

##### Penjelasan code random file lebih detail
Berdasarkan kode di atas, berikut ini adalah penjelasan kode secara detail:
```c
srand(time(NULL));
```
> Fungsi `time(NULL)` akan mengembalikan **waktu saat ini dalam detik** sejak Epoch (yaitu 1 Januari 1970, 00:00:00 UTC). Fungsi `srand()` memerlukan nilai **bilangan bulat** (disebut **seed**) dan menginisialisasi generator bilangan acak dengan titik awal berdasarkan nilai tersebut. Dengan memberikan `time(NULL)` sebagai seed ke `srand()`, generator bilangan acak akan diberi seed dengan **nilai yang kemungkinan akan berbeda** setiap kali program dijalankan, karena waktu saat ini akan selalu berbeda. Ini membantu menghasilkan angka acak yang berbeda setiap kali program dijalankan.

```c
DIR *dir = opendir("binatang_folder");
```
> Kode ini membuka direktori "binatang_folder" dan **mengembalikan pointer** ke struktur direktori yang disimpan dalam variabel `dir`. Setelah direktori dibuka, kita dapat membaca setiap file dan direktori dalam direktori ini menggunakan fungsi `readdir()`.

```c
struct dirent *entry;
```
> `struct dirent` adalah sebuah struct dalam bahasa C yang merepresentasikan sebuah **entry dalam directory**. Setiap entry dalam directory biasanya memiliki nama file, tipe file (reguler file, directory, symlink, dll.), dan beberapa atribut lainnya. **struct dirent** menyimpan informasi-informasi tersebut dalam bentuk data structure

> `struct dirent *entry` adalah sebuah **pointer** yang menunjuk ke suatu objek bertipe **struct dirent**. Variabel `entry` digunakan sebagai **placeholder** untuk menyimpan informasi mengenai **setiap entry** dalam directory `binatang_folder`. Setiap kali sebuah entry dibaca, informasi mengenai entry tersebut akan disimpan dalam variabel `entry`, sehingga nantinya dapat dilakukan manipulasi pada file-file tersebut.

```c
int file_count = 0;
```
> `int file_count = 0;` adalah deklarasi variabel **bertipe int** dengan nama `file_count` dan diinisialisasi dengan **nilai 0**. Variabel ini digunakan untuk **menghitung jumlah file** yang ditemukan dalam direktori "binatang_folder".

```c
 while ((entry = readdir(dir)) != NULL) {
                    if (entry->d_type == DT_REG) {
                        file_count++;
                        if (rand() % file_count == 0) {
                            sprintf(random_file_buf, "%s", entry->d_name);
                        }
                    }
                }
```
> Kode di atas adalah loop untuk membaca isi dari direktori "binatang_folder" dan memilih sebuah file secara acak

> Setiap file yang ditemukan akan diperiksa apakah itu adalah sebuah **file regular** menggunakan field `d_type` pada `struct dirent`. Jika iya, maka variabel `file_count` akan **bertambah 1** dan kemudian akan dipilih sebuah file secara acak menggunakan fungsi `rand()` dan operator **modulo** dengan `file_count`. Jika hasil modulo **sama dengan nol**, maka file tersebut akan dipilih dan namanya akan disimpan dalam variabel `random_file_buf` menggunakan fungsi `sprintf()`. 

```c
closedir(dir);
```
> Fungsi untuk **menutup directory stream** yang dibuka dengan fungsi `opendir()`. Ketika sebuah program selesai menggunakan directory stream, fungsi ini harus dipanggil untuk **menghindari memory leaks**. Setelah fungsi ini dipanggil, stream directory yang digunakan akan dihapus dari memory.

```c
printf("\033[1;36m\nFile gambar acak adalah: %s\033[0m\n\n", random_file_buf); 
```
> Kode ini adalah sebuah perintah `printf` yang mencetak output ke layar terminal. Outputnya adalah nama file gambar yang dipilih secara acak dari folder `binatang_folder`. Penggunaan `\033[1;36m` dan `\033[0m` dalam perintah ini adalah untuk **memberi warna** pada teks yang dicetak, di sini warna yang diberikan adalah warna **cyan**. `%s` pada perintah ini akan digantikan dengan **nama file gambar** yang diambil secara acak sebelumnya.


#### 4. Membuat direktori HewanDarat, HewanAmphibi, dan HewanAir
Setelah random file sudah dilakukan, selanjutnya kita harus membuat direktori HewanDarat, HewanAmphibi, dan HewanAir. Berikut ini adalah alur pengerjaan yang kami buat untuk melakukan tahapan ini:
1. Buat direktori HewanDarat. Jika gagal, keluarkan pesan error dan prgoram keluar
2. Buat direktori HewanAmphibi. Jika gagal, keluarkan pesan error dan prgoram keluar
3. Buat direktori HewanAir. Jika gagal, keluarkan pesan error dan prgoram keluar

Dari alur tersebut, dapat dibuat script dengan bahasa pemrograman C untuk **membuat tiga direktori** sebagai berikut:
```c
// create directory HewanDarat, HewanAmphibi, HewanAir    
                int status;
                status = mkdir("HewanDarat", 0777);
                if (status != 0) {
                    perror("mkdir failed");
                    exit(1);
                }

                status = mkdir("HewanAmphibi", 0777);
                if (status != 0) {
                    perror("mkdir failed");
                    exit(1);
                }

                status = mkdir("HewanAir", 0777);
                if (status != 0) {
                    perror("mkdir failed");
                    exit(1);
                }
```
##### Penjelasan code membuat tiga direktori lebih detail
Berdasarkan kode di atas, berikut ini adalah penjelasan kode secara detail:
```c
int status;
```
> Digunakan untuk **menyimpan status** dari fungsi `mkdir`. Variabel ini akan digunakan untuk mengecek apakah pembuatan direktori berhasil atau gagal. Jika nilai `status` **bukan 0**, maka direktori **gagal dibuat** dan program akan mengeluarkan pesan error. 

```c
status = mkdir("HewanDarat", 0777);
                if (status != 0) {
                    perror("mkdir failed");
                    exit(1);
                }
```
> Terdapat variabel `status` untuk menampung nilai status dari fungsi `mkdir()`. Fungsi `mkdir()` digunakan untuk membuat direktori dengan nama **"HewanDarat"** dan permission **0777**. Jika gagal dibuat maka dikeluarkan pesan "mkdir failed".

> Pada kode `status = mkdir("HewanDarat", 0777);`, angka **0777** menunjukkan permission yang diberikan untuk direktori yang baru dibuat. Permission **0777** pada direktori memungkinkan semua jenis akses yaitu **read, write, dan execute**. Dengan permission ini, pengguna dan grup pengguna memiliki **akses penuh ke direktori** tersebut dan pengguna yang berada di luar grup tersebut juga dapat mengakses direktori.

```c
status = mkdir("HewanAmphibi", 0777);
                if (status != 0) {
                    perror("mkdir failed");
                    exit(1);
                }
```
> Program membuat direktori baru dengan nama **"HewanAmphibi"** dengan menggunakan fungsi `mkdir()`. Variabel `status` digunakan untuk **menampung nilai yang dikembalikan** oleh fungsi `mkdir()`, yaitu **0** jika direktori **berhasil** dibuat dan **-1** jika terjadi **error**.

> Angka **0777** merupakan notasi **oktal** untuk memberikan hak akses **rwx** (read, write, execute) atau **full access** pada user, group, dan other.

> Jika direktori gagal dibuat, maka akan muncul pesan error "mkdir failed" yang dihasilkan oleh fungsi `perror()`, dan program akan keluar dengan **exit code 1** menggunakan fungsi `exit()`.

```c
status = mkdir("HewanAir", 0777);
                if (status != 0) {
                    perror("mkdir failed");
                    exit(1);
                }
```
> Kode ini membuat direktori dengan nama **"HewanAir"** dan mengatur permissionnya menjadi **0777**, yang memungkinkan akses **read, write, dan execute** bagi semua user. Jika pembuatan direktori gagal, maka program akan mencetak pesan error "mkdir failed" dan keluar dari program.

#### 5. Filter file ke direktori yang sesuai
Langkah selanjutnya setelah membuat tiga direktori adalah melakukan filter file ke direktori yang sesuai. Berikut ini adalah alur pengerjaan yang kami buat untuk melakukan tahap ini:
1. Mencari file dengan filter nama yang mengandung `_darat` dari folder `binatang_folder`
2. Jika file tersebut belum ada di dalam folder `HewanDarat`, maka file tersebut akan dipindahkan ke dalam folder `HewanDarat`
3. Lakukan hal yang sama untuk filter nama yang mengandung `_amphibi` dan `_air` untuk dipindahkan ke dalam folder `HewanAmphibi` dan `HewanAir`
4. Hapus folder `binatang_folder` beserta seluruh isinya untuk menghemat tempat karena sudah tidak digunakan lagi

Dari alur tersebut, dapat dibuat script dengan bahasa pemrograman C untuk **filter file ** ke direktori sebagai berikut:
```c
//filter file from binatang_folder
            //darat
                system("find . -name \"*_darat*\" -type f -exec sh -c 'for f; \
                        do if [ ! -e HewanDarat/\"$(basename \"$f\")\" ]; \
                        then mv \"$f\" HewanDarat/; \
                        fi; \
                        done' sh {} +");
            //amfibi
                system("find . -name \"*_amphibi*\" -type f -exec sh -c 'for f; \
                        do if [ ! -e HewanAmphibi/\"$(basename \"$f\")\" ]; \
                        then mv \"$f\" HewanAmphibi/; \
                        fi; \
                        done' sh {} +");
            // air
                system("find . -name \"*_air*\" -type f -exec sh -c 'for f; \
                do if [ ! -e HewanAir/\"$(basename \"$f\")\" ]; \
                then mv \"$f\" HewanAir/; \
                fi; \
                done' sh {} +");
            //remove binatang_folder
                system ("rm -rf binatang_folder");
```
##### Penjelasan code filter file lebih detail
Berdasarkan kode di atas, berikut ini adalah penjelasan kode secara detail:
```c
//darat
                system("find . -name \"*_darat*\" -type f -exec sh -c 'for f; \
                        do if [ ! -e HewanDarat/\"$(basename \"$f\")\" ]; \
                        then mv \"$f\" HewanDarat/; \
                        fi; \
                        done' sh {} +");
```
> Perintah ini digunakan untuk **mencari dan memindahkan file-file** yang memiliki string `_darat` pada nama filenya ke dalam direktori `HewanDarat`.

> `find . -name "*_darat*" -type f` : mencari file-file pada direktori saat ini (".") yang memiliki string `_darat` pada nama filenya dan berjenis **file** ("-type f")

> `-exec` : menjalankan perintah yang dituliskan pada setiap hasil pencarian

> `sh -c 'for f; do if [ ! -e HewanDarat/"$(basename "$f")" ]; then mv "$f" HewanDarat/; fi; done' sh {} +` : perintah shell yang dijalankan pada setiap hasil pencarian file. `for f; ` adalah loop untuk setiap hasil pencarian file, `if [ ! -e HewanDarat/"$(basename "$f")" ];` untuk mengecek apakah file dengan nama yang sama sudah ada di dalam direktori `HewanDarat`, `then mv "$f" HewanDarat/;` jika tidak ada, pindahkan file tersebut ke direktori `HewanDarat`, `fi;` akhir dari perintah if, `done` adalah akhir dari loop

> `sh {} +` : menutup perintah `sh -c` dan menambahkan argumen pada perintah shell tersebut. Dalam hal ini, `{}` akan diisi dengan** setiap hasil pencarian file** yang ditemukan oleh perintah find. Tanda `+` pada akhir perintah mengindikasikan bahwa satu perintah sh akan digunakan untuk menjalankan perintah pada **beberapa file sekaligus**.

```c
//amfibi
                system("find . -name \"*_amphibi*\" -type f -exec sh -c 'for f; \
                        do if [ ! -e HewanAmphibi/\"$(basename \"$f\")\" ]; \
                        then mv \"$f\" HewanAmphibi/; \
                        fi; \
                        done' sh {} +");
```
> Kode ini digunakan untuk memindahkan semua file dengan nama yang berisi `_amphibi` di dalam folder saat ini ke folder "HewanAmphibi`.

> Perintah `find` digunakan untuk mencari semua file dengan nama yang sesuai di dalam folder saat ini. Lalu, opsi `-type f` digunakan untuk memastikan bahwa yang dicari hanya **file** (bukan direktori). Setelah itu, opsi `-exec` digunakan untuk **mengeksekusi** sebuah perintah pada setiap file yang ditemukan.

> Perintah yang dieksekusi adalah sebuah **script shell**. Script ini akan mengambil setiap file yang ditemukan oleh `find` dan mengecek apakah file tersebut belum ada di dalam folder `HewanAmphibi`. Jika belum ada, maka file tersebut dipindahkan ke dalam folder tersebut menggunakan perintah `mv`.

```c
// air
                system("find . -name \"*_air*\" -type f -exec sh -c 'for f; \
                do if [ ! -e HewanAir/\"$(basename \"$f\")\" ]; \
                then mv \"$f\" HewanAir/; \
                fi; \
                done' sh {} +");
```
> Kode tersebut melakukan filter file yang mengandung kata `_air` di dalam direktori dan memindahkan file tersebut ke dalam direktori `HewanAir`.

> `find . -name \"*_air*\" -type f`: menemukan semua file yang mengandung kata `_air` di dalam direktori dan tipe file nya harus **file** (bukan direktori).

> `exec sh -c 'for f; ... sh {} +`: menjalankan perintah `sh -c 'for f; ...'` untuk setiap file yang ditemukan oleh `find`

> `for f; ...`: untuk setiap file yang ditemukan, dilakukan perintah `if [ ! -e HewanAir/\"$(basename \"$f\")\" ]; then mv \"$f\" HewanAir/; fi;` jika file tersebut belum ada di dalam direktori `HewanAir` (dengan menggunakan parameter `-e` pada perintah if dan fungsi basename pada parameter direktori) maka file tersebut dipindahkan ke direktori `HewanAir` dengan perintah `mv \"$f\" HewanAir/`. Kode `-e HewanAir/"$(basename "$f")\` berguna untuk memindahkan file yang sesuai ke folder HewanAir. Bagian `-e HewanAir/` menentukan bahwa file akan dipindahkan ke folder `HewanAir`, sedangkan `$(basename "$f")` mengambil **nama file** yang akan dipindahkan dan menambahkannya ke direktori tujuan.

```c
 //remove binatang_folder
                system ("rm -rf binatang_folder");
```
> Kode ini berfungsi untuk menghapus direktori `binatang_folder` beserta seluruh isinya menggunakan perintah `rm` dan opsi `-rf` yang berarti **recursive** dan **force**. Dengan opsi ini, direktori beserta isinya akan dihapus tanpa konfirmasi dan rekursif.

#### 6. Zip ketiga direktori tersebut
Langkah terakhir adalah kita harus melakukan zip ketiga folder yang sudah dibuat tersebut tanpa menggunakan fungsi `system()`. Berikut ini adalah alur pengerjaan yang kami buat untuk tahap ini:
1. Proses zip yang dilakukan menggunakan perintah yang sama sehingga dapat kita lakukan loop
2. Buat **array of string** untuk menyimpan nama folder yang akan di-zip dan nama file zip yang akan dihasilkan
3. Lakukan loop untuk setiap folder yang akan di-zip
4. Lakukan fork untuk tiap iterasi pada loop
5. Pada **child process**, gunakan fungsi untuk menjalankan **perintah zip** pada terminal dan membuat file zip dengan nama yang telah ditentukan
6. Jika zip berhasil, tampilkan pesan bahwa zip telah berhasil dan menghapus folder asli yang telah di-zip untuk menghemat tempat
7. Jika zip gagal, menampilkan pesan zip telah gagal
8. Pada **parent process**, tunggu child process selesai menjalankan perintah zip dan menampilkan pesan yang sesuai tergantung pada apakah zip berhasil atau gagal

Dari alur tersebut, dapat dibuat script dengan bahasa pemrograman C untuk **zip tiga direktori** sebagai berikut:
```c
//fork 3 - zip the folders
                char *type[]= {"HewanAir","HewanAmphibi", "HewanDarat"};
                char *zipName[] = {"HewanAir.zip","HewanAmphibi.zip", "HewanDarat.zip"};
                int size = sizeof(type) / sizeof(type[0]);
                pid_t pid3;
                for (int i=0; i<size; i++)
                {
                    pid3 = fork();
                    if (pid3 < 0) {
                        fprintf(stderr, "Fork 3 failed");
                    } else if (pid3 == 0) {
                        execlp("zip", "zip", "-r", zipName[i], type[i], NULL);
                        perror("exec failed");
                        exit(1);
                    } else {
                        wait(&status);
                        if (WIFEXITED(status) && !WEXITSTATUS(status)) {
                            printf("%s completed successfully\n", zipName[i]);
                            //remove the original folder before zipping
                            char cmd[100];
                            sprintf(cmd, "rm -rf %s", type[i]);
                            system(cmd);
                        }
                        else {
                            printf ("Zip failed\n");
                        }
                    }
                }

            } else {
                printf("Unzip failed\n");
            }
```
##### Penjelasan code zip direktori lebih detail
Berdasarkan kode di atas, berikut ini adalah penjelasan kode secara detail:
```c
char *type[]= {"HewanAir","HewanAmphibi", "HewanDarat"};
char *zipName[] = {"HewanAir.zip","HewanAmphibi.zip", "HewanDarat.zip"};
```

> Baris tersebut mendefinisikan `array type` yang berisi **nama folder yang akan di-zip** dan `array zipName` yang berisi **nama** untuk masing-masing file **hasil kompresi zip**.`array type` berisi "HewanAir", "HewanAmphibi", dan "HewanDarat", sedangkan array `zipName` berisi "HewanAir.zip", "HewanAmphibi.zip", dan "HewanDarat.zip".

```c
int size = sizeof(type) / sizeof(type[0]);
```
> Baris ini menghitung jumlah elemen dalam `array type` dan membaginya dengan ukuran satu elemen dalam `array type` untuk mengetahui berapa jumlah elemen dalam array tersebut. Hasilnya kemudian disimpan ke dalam variabel `size`.

```c
pid_t pid3;
```
> Variabel `pid3` merupakan tipe data `pid_t` yang digunakan untuk menyimpan nilai dari **proses ID (PID)** dari proses yang sedang dijalankan dengan `fork()`. Variabel ini digunakan dalam proses `for loop` untuk memastikan bahwa kode berjalan pada proses anak.

```c
for (int i=0; i<size; i++)
                {
                    pid3 = fork();
                    if (pid3 < 0) {
                        fprintf(stderr, "Fork 3 failed");
                    } else if (pid3 == 0) {
                        execlp("zip", "zip", "-r", zipName[i], type[i], NULL);
                        perror("exec failed");
                        exit(1);
                    } else {
                        wait(&status);
                        if (WIFEXITED(status) && !WEXITSTATUS(status)) {
                            printf("%s completed successfully\n", zipName[i]);
                            //remove the original folder before zipping
                            char cmd[100];
                            sprintf(cmd, "rm -rf %s", type[i]);
                            system(cmd);
                        }
                        else {
                            printf ("Zip failed\n");
                        }
                    }
                }
```

> Kode ini melakukan loop sebanyak `size` kali dimana `size` adalah jumlah elemen dalam `array type` dan `zipName`. Setiap iterasi, program melakukan **fork** dan mengeksekusi perintah zip untuk mengompres folder yang sesuai dengan tipe hewan (air, amfibi, atau darat) ke dalam file zip yang sesuai dengan nama array `zipName`

> Jika fork gagal, maka akan dicetak pesan error. Jika tidak, maka proses anak akan menjalankan perintah zip menggunakan fungsi `execlp()` dan mengekompres folder sesuai dengan tipe hewan dan menyimpannya dalam file zip yang sesuai. Jika proses anak selesai berhasil, maka folder asli akan dihapus menggunakan perintah `rm -rf`. Jika terjadi kesalahan saat melakukan kompresi atau proses keluar dengan kode error, maka akan dicetak pesan kesalahan yang sesuai. Setelah semua folder telah dikompresi dan dihapus, program selesai

Berikut ini adalah penjelasan for-loop tiap baris yang dilakukan:

```c
if (pid3 < 0) {
    fprintf(stderr, "Fork 3 failed");
}
```
> Baris kode ini mengecek apakah fork gagal. Jika gagal, program akan mencetak pesan kesalahan "Fork 3 failed" ke **stderr**.

```c
else if (pid3 == 0) {
                        execlp("zip", "zip", "-r", zipName[i], type[i], NULL);
                        perror("exec failed");
                        exit(1);
                    }
```

> Pada bagian ini, program akan melakukan **zip** pada folder dengan memanggil perintah `zip` dari sistem dan mengirim argumen-argumen berupa `-r` (**recursive**, artinya zip secara rekursif) dan nama folder yang akan di-zip, kemudian nama file output zip yang diinginkan. Jika eksekusi error keluarkan pesan "exec failed"

> Fungsi `execlp()` pada bagian ini digunakan untuk menjalankan perintah `zip` pada command line, di mana argumen pertama `zip` merupakan nama perintah yang akan dijalankan, `zip` juga merupakan argumen kedua yang menjelaskan **nama program** yang dijalankan, argumen selanjutnya adalah argumen-argumen perintah zip, yaitu `-r`, nama folder yang akan di-zip (`type[i]`), dan `NULL`

> Jika terjadi kesalahan saat melakukan `execlp()`, program akan menampilkan **pesan error** dan mengakhiri child process dengan nilai **exit 1**.

```c
else {
                        wait(&status);
                        if (WIFEXITED(status) && !WEXITSTATUS(status)) {
                            printf("%s completed successfully\n", zipName[i]);
                            //remove the original folder before zipping
                            char cmd[100];
                            sprintf(cmd, "rm -rf %s", type[i]);
                            system(cmd);
                        }
                        else {
                            printf ("Zip failed\n");
                        }
                    }
```
> Setelah proses zip folder berhasil dilakukan oleh child process, maka **parent process** akan menunggu child process tersebut untuk selesai dengan menggunakan fungsi `wait()`

> Kemudian, **parent process** akan mengecek status keluaran dari child process dengan menggunakan fungsi `WIFEXITED()` dan `WEXITSTATUS()`. Jika **status** keluaran dari child process adalah **0**, maka zip folder tersebut dianggap **berhasil** dan parent process akan mencetak pesan "Hewan[Type].zip completed successfully" dengan [Type[i]] diganti dengan jenis hewan yang sedang di-zip

> Parent process akan **menghapus folder original** yang telah di-zip dengan menggunakan perintah `rm -rf [Type[i]]`. Jika **status** keluaran dari child process **bukan 0**, maka zip folder tersebut dianggap **gagal** dan parent process akan mencetak pesan "Zip failed".

#### Output Program
Saat program di run, maka akan langsung melakukan tahapan-tahapan yang telah dijelaskan di atas. Output program yang diperoleh keseluruhan sebagai berikut:

![full-a](img/1_full-a.png)

![full-b](img/1_full-b.png)

Pada akhir program, hanya tersisa 3 file zip yaitu HewanAir.zip, Hewan Amphibi.zip, dan HewanDarat.zip. Hal ini dikarenakan folder lain (seperti file zip download original, direktori HewanAir, HewanAmphibi, dan HewanDarat) sudah dihapus dalam kode ini untuk menghemat tempat.

![dir](img/1_dir.png)

Berikut ini adalah _breakdown_ hasil screenshot output program:
1. Melakukan download file zip dari link drive

![download-zip](img/1_download-zip.png)

2. Melakukan unzip

![unzip](img/1_unzip.png)

3. Memilih random file

![random-a](img/1_random-a.png)

4. Membuat 3 direktori, Filter file, dan zip direktori

![last](img/1_last.png)

Untuk memastikan random file bekerja benar-benar secara random pada saat waktu run yang berbeda, kami lakukan **uji coba running beberapa kali** yang diperoleh output sebagai berikut:

1. Running ke-1

![run-a](img/1-run-a.png)

File gambar acak : **shark_air.jpg**

2. Running ke-2

![run-b](img/1-run-b.png)

File gambar acak : **singa_darat.jpg**

3. Running ke-3

![run-c](img/1-run-c.png)

File gambar acak : **lintah_amphibi.jpg**

4. Running ke-4

![run-d](img/1-run-d.png)

File gambar acak : **komodo_darat.jpg**

5. Running ke-5

![run-e](img/1-run-e.png)

File gambar acak : **buaya_amphibi.jpg**

Dapat dilihat bahwa hasil gambar acak yang diperoleh selalu berbeda setiap running sehingga kode ini berhasil

#### Kendala Pengerjaan

Dalam mengerjakan soal ini, kami sempat memiliki kendala saat langkah terakhir soal, yaitu melakukan zip ketiga direktori. Kami ingin melakukan iterasi untuk melakukan zip tersebut karena program yang dilakukan sama, sehingga pada awalnya kami menyimpan hanya `char *type[]` untuk nama direktori. Untuk `zipName` pada awalnya kami buat langsung pada `execlp` dengan hanya menambahkan ekstensi `.zip` pada `type[i]` namun saat di-run program tidak melakukan seperti yang di-ekspektasikan. Saat kami membuat variable baru untuk menyimpan zipName pada `char *zipName[]` kode baru bisa berhasil untuk melakukan zip. 

Rupanya hal ini terjadi karena `type[i].zip` bukanlah sebuah variabel atau array yang ada di dalam program. `type[i]` adalah sebuah string yang berisi nama file zip yang ingin dihasilkan, sedangkan `.zip` bukanlah bagian dari string tersebut, melainkan hanya sebuah ekstensi file. Cara lain untuk membuat program ini bekerja adalah dengan menambahkan ekstensi `.zip` pada nama file zip menggunakan fungsi `strcat()` atau dengan membuat string baru untuk menyimpan nama file zip dengan ekstensi `.zip`.

## 🎨 Nomor 2 
### Soal
Sucipto adalah seorang seniman terkenal yang berasal dari Indonesia. Karya nya sudah terkenal di seluruh dunia, dan lukisannya sudah dipajang di berbagai museum mancanegara. Tetapi, akhir-akhir ini sucipto sedang terkendala mengenai ide lukisan ia selanjutnya. Sebagai teman yang jago sisop, bantu sucipto untuk melukis dengan mencarikannya gambar-gambar di internet sebagai referensi !
- Pertama-tama, buatlah sebuah folder khusus, yang dalamnya terdapat sebuah program C yang per 30 detik membuat sebuah folder dengan nama timestamp [YYYY-MM-dd_HH:mm:ss].
- Tiap-tiap folder lalu diisi dengan 15 gambar yang di download dari https://picsum.photos/ , dimana tiap gambar di download setiap 5 detik. Tiap gambar berbentuk persegi dengan ukuran (t%1000)+50 piksel dimana t adalah detik Epoch Unix. Gambar tersebut diberi nama dengan format timestamp [YYYY-mm-dd_HH:mm:ss].
- Agar rapi, setelah sebuah folder telah terisi oleh 15 gambar, folder akan di zip dan folder akan di delete(sehingga hanya menyisakan .zip).
- Karena takut program tersebut lepas kendali, Sucipto ingin program tersebut men-generate sebuah program "killer" yang siap di run(executable) untuk menterminasi semua operasi program tersebut. Setelah di run, program yang menterminasi ini lalu akan mendelete dirinya sendiri.
- Buatlah program utama bisa dirun dalam dua mode, yaitu MODE_A dan MODE_B. untuk mengaktifkan MODE_A, program harus dijalankan dengan argumen -a. Untuk MODE_B, program harus dijalankan dengan argumen -b. Ketika dijalankan dalam MODE_A, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan. Untuk MODE_B, ketika program killer dijalankan, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai(semua folder terisi gambar, terzip lalu di delete).

Catatan :
- Tidak boleh menggunakan system()
- Proses berjalan secara daemon
- Proses download gambar pada beberapa folder dapat berjalan secara bersamaan (overlapping)

#### List penyelesaian:
1. Terdapat lukisan.c 
2. Program tersebut dapat membuat folder baru setiap 30 detik dengan nama timestamp [YYYY-MM-dd_HH:mm:ss]
3. Tiap folder yang telah terbentuk diisi 15 gambar yang didownload dari https://picsum.photos/ dan tiap gambar didownload tiap 5 detik
4. Tiap gambar yang didownload berbentuk persegi dengan ukuran (t%1000)+50 piksel dimana t adalah detik Epoch Unix
5. Tiap gambar diberi penamaan [YYYY-mm-dd_HH:mm:ss]
6. Setelah folder terisi 15 gambar, folder akan di zip dan auto delete folder sebelum ter zip 
7. Terdapat program killer yang siap di run untuk menterminasi semua operasi program lalu akan mendelete dirinya sendiri 
8. Terdapat program utama yang dapat dijalankan 2 mode yaitu MODE_A dan MODE_B 
- MODE_A = program harus dijalankan dengan argumen -a. Fungsinya adalah akan langsung menghentikan semua operasinya ketika program killer dijalankan 
- MODE_B = Program harus dijalankan dengan argumen -b. Ketika program killer dijalankan, program utama akan berhenti tetapi membiarkan proses di setiap folder agar masih berjalan hingga selesai (semua folder terisi gambar, terzip, lalu di delete).

### Jawaban 
Dari list penyelesaian tersebut, codingan penyelesaian yang terbentuk adalah 

``` c
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <time.h>
#include <sys/wait.h>
#include <string.h>
#include <sys/stat.h>
#include <signal.h>

char dirr[20];
int stop = 0;

void download_img(){
    char buff[100];

    //digunakan untuk penamaan img
    time_t now = time(NULL);
    strftime(buff, 20, "%Y-%m-%d_%H:%M:%S", localtime(&now));

    //penamaan img
    char name_img[150];
    sprintf(name_img, "%s.jpeg", buff);

    //mengambil seconds
    time_t seconds;
    seconds = time(NULL);

    //menetapkan ukuran
    int ukuran = (seconds % 1000) + 50;

    //mengambil link url dengan ukuran yang telah disesuaikan
    char link_img[100];
    sprintf(link_img, "https://picsum.photos/%d", ukuran);

    if(fork() == 0){
        //akan download gambar
        char *gmbr[5] = {"wget", "-qO", name_img, link_img, NULL};
        execv("/usr/bin/wget", gmbr);
    }
}


void make_dir(){

    // membuat directory baru 
    char *argvp[3] = {"mkdir", dirr, NULL};
    execvp("mkdir", argvp);
}


int main(int argc, char *argv[]){

    if(argc != 2){

        puts("Argument is not valid");
        exit(0);
    }

    pid_t pid, sid;

    //membuat parent id
    pid = fork();

    if(pid < 0){
        exit(EXIT_FAILURE);
    }

    if(pid > 0){
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();

    if(sid < 0){
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    //handle untuk inputan ./lukisan -a
    if (strcmp(argv[1], "-a") == 0){

        FILE *fPtr = NULL;
        fPtr = fopen("killer.c", "w");

        fprintf(fPtr, "#include <stdio.h>\n");
        fprintf(fPtr, "#include <stdlib.h>\n");
        fprintf(fPtr, "#include <unistd.h>\n");
        fprintf(fPtr, "#include <sys/wait.h>\n\n");
        fprintf(fPtr, "int main(int argc, char *argv[]) {\n");
        fprintf(fPtr, "char* args[] = {\"pkill\", \"lukisan\", NULL};\n");
        fprintf(fPtr, "pid_t pid = fork();\n\n");
        fprintf(fPtr, "if (pid == 0) {\n");
        fprintf(fPtr, "\t// Child process\n");
        fprintf(fPtr, "\texecvp(\"pkill\", args);\n");
        fprintf(fPtr, "} else if (pid > 0) {\n");
        fprintf(fPtr, "\t// Parent process\n");
        fprintf(fPtr, "\twait(NULL);\n");
        fprintf(fPtr, "\tremove(\"killer.c\");\n");
        fprintf(fPtr, "\tremove(\"killer\");\n");
        fprintf(fPtr, "} else {\n");
        fprintf(fPtr, "\t// Forking error\n");
        fprintf(fPtr, "\tprintf(\"Failed to fork process\\n\");\n");
        fprintf(fPtr, "}\n");
        fprintf(fPtr, "return 0; \n}");

        fclose(fPtr);

        pid_t fid = fork();

        if (fid == -1) {
            printf("Failed to fork process\n");
        } 
        
        // akan membuat killer.c telah dicompile
        if (fid == 0) {
            // Child process
            char* args[] = {"gcc", "killer.c", "-o", "killer", NULL};
            execvp("gcc", args);
            printf("Failed to compile killer.c\n");
        } else {
            // Parent process
            wait(NULL);
        }
    }

    //handle untuk inputan ./lukisan -b
    else if (strcmp(argv[1], "-b") == 0){

        FILE *fPtr = NULL;
        fPtr = fopen("killer.c", "w");

        int id = getpid();

        fprintf(fPtr, "#include <stdio.h>\n");
        fprintf(fPtr, "#include <stdlib.h>\n");
        fprintf(fPtr, "#include <unistd.h>\n");
        fprintf(fPtr, "#include <signal.h>\n");
        fprintf(fPtr, "#include <sys/wait.h>\n\n");
        fprintf(fPtr, "int main() {\n");
        fprintf(fPtr, "pid_t id2 = %d;\n", id);
        fprintf(fPtr, "pid_t pid = fork();\n\n");
        fprintf(fPtr, "if (pid == 0) {\n");
        fprintf(fPtr, "\t// Child process\n");
        fprintf(fPtr, "\tkill(id2, SIGTERM);\n");
        fprintf(fPtr, "} else if (pid > 0) {\n");
        fprintf(fPtr, "\t// Parent process\n");
        fprintf(fPtr, "\twait(NULL);\n");
        fprintf(fPtr, "\tremove(\"killer.c\");\n");
        fprintf(fPtr, "\tremove(\"killer\");\n");
        fprintf(fPtr, "} else {\n");
        fprintf(fPtr, "\t// Forking error\n");
        fprintf(fPtr, "\tprintf(\"Failed to fork process\\n\");\n");
        fprintf(fPtr, "}\n");
        fprintf(fPtr, "return 0; \n}");

        fclose(fPtr);

        pid_t fid = fork();

        if (fid == -1) {
            printf("Failed to fork process\n");
        } 
        
        //akan membuat killer.c telah dicompile
        if (fid == 0) {
            // Child process
            char* args[] = {"gcc", "killer.c", "-o", "killer", NULL};
            execvp("gcc", args);
            printf("Failed to compile killer.c\n");
        } else {
            // Parent process
            wait(NULL);
        }

    }

    while(1){

        //get time sekarang untuk penamaan directory
        time_t now = time(NULL);
        strftime(dirr, 20, "%Y-%m-%d_%H:%M:%S", localtime(&now));


        pid_t id;
        id = fork();

        //membuat directory setiap 30 detik
        if (id == 0){ //child process

            make_dir(dirr);

        }

        //menunggu pembuatan directory selama 1 detik
        sleep(1);

        pid_t id2;
        id2 = fork();

        //terjadi download image dan mengubah current directory menjadi zip setelah download 15 gambar
        if(id2 == 0) { //child process

            chdir(dirr);

            for(int i = 1; i <= 15; i++){

                download_img();

                puts("Download image lagi sleeping");

                sleep(5);

                puts("Lanjut download image");
            }
            
            chdir("..");

            char name_zip[100];
            sprintf(name_zip, "%s.zip", dirr);

            char  *fol_zip[5] = {"zip", "-rm", name_zip, dirr, NULL};
            execvp("zip", fol_zip);
              
            exit(0);
        }

        //akan sleep selama 29 detik karena 1 detiknya dibuat untuk membuat directory
        sleep(29);
    }

    return EXIT_SUCCESS;

}
```

### Penjelasain lebih detail mengenai code 

1. Pertama-tama, kita declare isi lukisan.c berikut sebagai sebuah daemon process. Sehingga, terdapat:

``` c

int main(){

    pid_t pid, sid;

    //membuat parent id
    pid = fork();

    if(pid < 0){
        exit(EXIT_FAILURE);
    }

    if(pid > 0){
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();

    if(sid < 0){
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    while(1){

         //program yang akan dijalankan
    }

    return EXIT_SUCCESS;
}

```

> Disini kita membuat parent id yang bernama `pid` lalu kita lakukan fork(). Sehingga, untuk parent process, akan menghasilkan `exit(EXIT_SUCCESS)`

> Lalu kita setup file permission untuk daemon process ini agar bisa akses full dengan `umask(0)`

> Lalu, untuk child process kita tetapkan unique id `sid = setsid()`. Hal tersebut bertujuan agar pada saat parent process di kill, program tidak akan menjadi orphan process. 

> Lalu, karena sebuah daemon tidak boleh menggunakan terminal, maka kita harus menutup file descriptor standar (STDIN, STDOUT, STDERR). 

``` c
close(STDIN_FILENO);
close(STDOUT_FILENO);
close(STDERR_FILENO);
```

> Lalu terdapat while loop yang berisi program yang akan dijalankan 

2. Dapat terbentuk folder baru setiap 30 detik dengan nama timestamp [YYYY-MM-dd_HH:mm:ss]

> Pertama-tama, dalam while loop, kita get time sekarang untuk penamaan folder 

``` c
//get time sekarang untuk penamaan directory
time_t now = time(NULL);
strftime(dirr, 20, "%Y-%m-%d_%H:%M:%S", localtime(&now));
```

> Lalu kita membuat process yang memanggil fork(). Fungsinya adalah untuk melakukan job membuat directory dan parent processnya akan menunggu 29 detik sebelum membuat directory baru lagi 

``` c
    while(1){

        //get time sekarang untuk penamaan directory
        time_t now = time(NULL);
        strftime(dirr, 20, "%Y-%m-%d_%H:%M:%S", localtime(&now));


        pid_t id;
        id = fork();

        if (id == 0){ //child process

            //akan ada fungsi untuk membuat directory 

        }

        //menunggu pembuatan directory selama 1 detik
        sleep(1);

        // program lain yang nanti dijelaskan

        //akan sleep selama 29 detik karena 1 detiknya dibuat untuk membuat directory
        sleep(29);
    }

```

> Kita buat sebuah function bernama `make_dir` yang berisi:

``` c
void make_dir(){

    // membuat directory baru 
    char *argvp[3] = {"mkdir", dirr, NULL};
    execvp("mkdir", argvp);
}
```

disini, dijalankan process untuk mengeksekusi perintah `mkdir` menggunakan `execvp`
penamaan  `mkdir` berasal dari directory global bernama `dirr` yang berisi timestamp sekarang. 

> Lalu, function `mkdir` akan berjalan didalam child process 

``` c
    while(1){

        //get time sekarang untuk penamaan directory
        time_t now = time(NULL);
        strftime(dirr, 20, "%Y-%m-%d_%H:%M:%S", localtime(&now));


        pid_t id;
        id = fork();

        //membuat directory setiap 30 detik
        if (id == 0){ //child process

            make_dir(dirr);

        }

        //menunggu pembuatan directory selama 1 detik
        sleep(1);

        // program lain yang nanti dijelaskan

        //akan sleep selama 29 detik karena 1 detiknya dibuat untuk membuat directory
        sleep(29);
    }

```

3. Tiap folder yang telah terbentuk akan diisi 15 gambar yang didownload dari https://picsum.photos/ dan tiap gambar didownload tiap 5 detik lalu Tiap gambar yang didownload berbentuk persegi dengan ukuran (t%1000)+50 piksel dimana t adalah detik Epoch Unix dan Tiap gambar diberi penamaan [YYYY-mm-dd_HH:mm:ss]

maka: 

> Kita buat process yang melakukan fork() lagi dengan variabel `id2` dengan tujuan child process berfungsi untuk mendownload image didalam folder sebanyak 15 kali, setelah itu dia akan melakukan zip. Parent processnya akan menunggu `sleep(29)`

``` c
    while(1){

        //get time sekarang untuk penamaan directory
        time_t now = time(NULL);
        strftime(dirr, 20, "%Y-%m-%d_%H:%M:%S", localtime(&now));


        pid_t id;
        id = fork();

        //membuat directory setiap 30 detik
        if (id == 0){ //child process

            make_dir(dirr);

        }

        //menunggu pembuatan directory selama 1 detik
        sleep(1);

        pid_t id2;
        id2 = fork();

        //terjadi download image dan mengubah current directory menjadi zip setelah download 15 gambar
        if(id2 == 0) { //child process

            chdir(dirr);

            for(int i = 1; i <= 15; i++){

                download_img();

                puts("Download image lagi sleeping");

                sleep(5);

                puts("Lanjut download image");
            }
            
            chdir("..");

            char name_zip[100];
            sprintf(name_zip, "%s.zip", dirr);

            char  *fol_zip[5] = {"zip", "-rm", name_zip, dirr, NULL};
            execvp("zip", fol_zip);
              
            exit(0);
        }

        //akan sleep selama 29 detik karena 1 detiknya dibuat untuk membuat directory
        sleep(29);
    }

```

> function `download_img()` berisi:

``` c
void download_img(){
    char buff[100];

    //digunakan untuk penamaan img
    time_t now = time(NULL);
    strftime(buff, 20, "%Y-%m-%d_%H:%M:%S", localtime(&now));

    //penamaan img
    char name_img[150];
    sprintf(name_img, "%s.jpeg", buff);

    //mengambil seconds
    time_t seconds;
    seconds = time(NULL);

    //menetapkan ukuran
    int ukuran = (seconds % 1000) + 50;

    //mengambil link url dengan ukuran yang telah disesuaikan
    char link_img[100];
    sprintf(link_img, "https://picsum.photos/%d", ukuran);

    if(fork() == 0){
        //akan download gambar
        char *gmbr[5] = {"wget", "-qO", name_img, link_img, NULL};
        execv("/usr/bin/wget", gmbr);
    }
}

```

> Lalu function tersebut berada dalam forloop sebanyak 15 kali dengan `sleep(5)`

``` c
for(int i = 1; i <= 15; i++){

    download_img();

    puts("Download image lagi sleeping");

    sleep(5);

    puts("Lanjut download image");
}
```

> Lalu didalam child process terjadi process zip 

``` c
char name_zip[100];
sprintf(name_zip, "%s.zip", dirr);

char  *fol_zip[5] = {"zip", "-rm", name_zip, dirr, NULL};
execvp("zip", fol_zip);

```

4. Kita buat program killer yang apabila MODE_A dijalankan maka akan langsung menghentikan semua operasinya dan apabila MODE_B dijalankan maka program utama akan berhenti, akan teteapi membiarkan proses di setiap folder agar masih berjalan hingga selesai (semua folder terisi gambar, terzip, lalu di delete)

> Kita buat dulu untuk menghandle apabila inputan dari user tidak terdapat `./lukisan -a` ataupun `./lukisan -b` karena mode A berjalan dengan `./lukisan -a` dan mode B berjalan dengan `./lukisan -b`

``` c
if(argc != 2){

    puts("Argument is not valid");
    exit(0);
}
```

> Untuk mode A maka: 

``` c 

if (strcmp(argv[1], "-a") == 0){

    FILE *fPtr = NULL;
    fPtr = fopen("killer.c", "w");

    fprintf(fPtr, "#include <stdio.h>\n");
    fprintf(fPtr, "#include <stdlib.h>\n");
    fprintf(fPtr, "#include <unistd.h>\n");
    fprintf(fPtr, "#include <sys/wait.h>\n\n");
    fprintf(fPtr, "int main(int argc, char *argv[]) {\n");
    fprintf(fPtr, "char* args[] = {\"pkill\", \"lukisan\", NULL};\n");
    fprintf(fPtr, "pid_t pid = fork();\n\n");
    fprintf(fPtr, "if (pid == 0) {\n");
    fprintf(fPtr, "\t// Child process\n");
    fprintf(fPtr, "\texecvp(\"pkill\", args);\n");
    fprintf(fPtr, "} else if (pid > 0) {\n");
    fprintf(fPtr, "\t// Parent process\n");
    fprintf(fPtr, "\twait(NULL);\n");
    fprintf(fPtr, "\tremove(\"killer.c\");\n");
    fprintf(fPtr, "\tremove(\"killer\");\n");
    fprintf(fPtr, "} else {\n");
    fprintf(fPtr, "\t// Forking error\n");
    fprintf(fPtr, "\tprintf(\"Failed to fork process\\n\");\n");
    fprintf(fPtr, "}\n");
    fprintf(fPtr, "return 0; \n}");

    fclose(fPtr);

    pid_t fid = fork();

    if (fid == -1) {
        printf("Failed to fork process\n");
    } 
    
    // akan membuat killer.c telah dicompile
    if (fid == 0) {
        // Child process
        char* args[] = {"gcc", "killer.c", "-o", "killer", NULL};
        execvp("gcc", args);
        printf("Failed to compile killer.c\n");
    } else {
        // Parent process
        wait(NULL);
    }
}

```
program tersebut akan auto membuat file killer.c 

``` c
FILE *fPtr = NULL;
fPtr = fopen("killer.c", "w"); //statusnya write
```

yang akan berisi syntax untuk kill all process dengan `pkill` lalu akan auto remove killer.c dan compiler killer dengan `remove("killer.c")` dan juga `remove("killer")`

Setelah terbentuk file killer.c tersebut, maka kita buat fork() baru yang dapat auto compile untuk file killer. c dengan 

``` c
// akan membuat killer.c telah dicompile
if (fid == 0) {
    // Child process
    char* args[] = {"gcc", "killer.c", "-o", "killer", NULL};
    execvp("gcc", args);
    printf("Failed to compile killer.c\n");
} else {
    // Parent process
    wait(NULL);
}
```

> Untuk mode B, maka: 

``` c
else if (strcmp(argv[1], "-b") == 0){

    FILE *fPtr = NULL;
    fPtr = fopen("killer.c", "w");

    int id = getpid();

    fprintf(fPtr, "#include <stdio.h>\n");
    fprintf(fPtr, "#include <stdlib.h>\n");
    fprintf(fPtr, "#include <unistd.h>\n");
    fprintf(fPtr, "#include <signal.h>\n");
    fprintf(fPtr, "#include <sys/wait.h>\n\n");
    fprintf(fPtr, "int main() {\n");
    fprintf(fPtr, "pid_t id2 = %d;\n", id);
    fprintf(fPtr, "pid_t pid = fork();\n\n");
    fprintf(fPtr, "if (pid == 0) {\n");
    fprintf(fPtr, "\t// Child process\n");
    fprintf(fPtr, "\tkill(id2, SIGTERM);\n");
    fprintf(fPtr, "} else if (pid > 0) {\n");
    fprintf(fPtr, "\t// Parent process\n");
    fprintf(fPtr, "\twait(NULL);\n");
    fprintf(fPtr, "\tremove(\"killer.c\");\n");
    fprintf(fPtr, "\tremove(\"killer\");\n");
    fprintf(fPtr, "} else {\n");
    fprintf(fPtr, "\t// Forking error\n");
    fprintf(fPtr, "\tprintf(\"Failed to fork process\\n\");\n");
    fprintf(fPtr, "}\n");
    fprintf(fPtr, "return 0; \n}");

    fclose(fPtr);

    pid_t fid = fork();

    if (fid == -1) {
        printf("Failed to fork process\n");
    } 
    
    //akan membuat killer.c telah dicompile
    if (fid == 0) {
        // Child process
        char* args[] = {"gcc", "killer.c", "-o", "killer", NULL};
        execvp("gcc", args);
        printf("Failed to compile killer.c\n");
    } else {
        // Parent process
        wait(NULL);
    }

}

```

Disini juga akan terbentuk file killer.c dengan 

``` c
FILE *fPtr = NULL;
fPtr = fopen("killer.c", "w"); //statusnya write
```

Lalu killer.c ini akan berisi syntax untuk membunuh program utama tapi proses yang masih berjalan dapat selesai hingga terzip dan terdelete. Oleh karena itu, digunakan `kill(id2, SIGTERM)` dengan id2 yang berasal dari `lukisan.c` dan kita erase file killer.c dengan `remove("killer.c")` lalu kita erase juga compilernya `remove("killer")`

Karena kita ingin agar killer.c auto compile dirinya, maka kita buat fork() baru untuk membuat perintah compile gcc dengan 

```c
//akan membuat killer.c telah dicompile
if (fid == 0) {
    // Child process
    char* args[] = {"gcc", "killer.c", "-o", "killer", NULL};
    execvp("gcc", args);
    printf("Failed to compile killer.c\n");
} else {
    // Parent process
    wait(NULL);
}
```

### Output Program

- MODE_A
1. Kita lakukan compile dulu untuk `lukisan.c`

![compile pertama](./img/compile_gcc.png)

2. Terbentuk compiler `lukisan` 

![compiler](./img/compiler.png)

3. Download gambar tiap 5 detik di suatu folder 

![gambar](./img/gambartiap5detik.png)

4. Terbentuk folder tiap 30 detik: (ada salah satu telah terzip)

![folder](./img/folderTiap30detik.png)

5. Melihat semua proses yang sedang berjalan pada `./lukisan`

![proses berjalan ](./img/semuaprosesyangberjalan.png)

6. Menjalankan program killer 

![killer](./img/menjalankanCompilerkiller.png)

7. Proses yang berjalan sekarang dengan `grep lukisan`

![proses sekarang ](./img/proseslukisanskrg.png)

- MODE_B

1. Kita lakukan pemanggilan untuk mode B: 

![mode B ](./img/memanggilmodeb.png)

2. Telah terbentuk 2 folder yang prosesnya sedang running: 

![proses folder ](./img/terbentuk2folder.png)

3. Memanggil program killer 

![proses folder ](./img/memanggilkiller.png)

4. Terlihat bahwa program utama mati pada `ps aux | grep lukisan`

![program utama mati ](./img/programutamamati.png)

5. Proses masih berjalan sehingga folder menit 23.17 terzip 

![23.17 terzip  ](./img/foldermenit23.17terzip.png)

6. Pada `ps aux | grep lukisan` tinggal tersisa 1 proses untuk melakukan zip pada folder 23.18

![23.18 process  ](./img/tinggal1proses.png)

7. Folder pada menit 23.18 pun terzip 

![23.18 terzip  ](./img/program23.18terzip.png)

8. Semua proses telah selesai 

![finish  ](./img/semuaprosesselesai.png)


### Kendala Pengerjaan

Kendala yang dialami oleh kelompok kami selama pengerjaan soal 2 adalah dibutuhkan algoritma yang tepat dan dibutuhkan penggunaan fork() yang baik agar img dapat terdownload dalam folder (karena sebelumnya kami harus menangani img yang tidak terdownload dalam folder, melainkan di current directory)


## ⚽ Nomor 3
### Soal
Ten Hag adalah seorang pelatih Ajax di Liga Belanda. Suatu hari, Ten Hag mendapatkan tawaran untuk menjadi manajer Manchester United. Karena Ten Hag masih mempertimbangkan tawaran tersebut, ia ingin mengenal para pemain yang akan dilatih kedepannya. Dikarenakan Ten Hag hanya mendapatkan url atau link database mentah para pemain bola, maka ia perlu melakukan klasifikasi pemain Manchester United. Bantulah Ten Hag untuk mengenal para pemain Manchester United tersebut hanya dengan 1 Program C bernama “filter.c”
- Pertama-tama, Program filter.c akan mengunduh file yang berisikan database para pemain bola. Kemudian dalam program yang sama diminta dapat melakukan extract “players.zip”. Lalu hapus file zip tersebut agar tidak memenuhi komputer Ten Hag.
- Dikarenakan database yang diunduh masih data mentah. Maka bantulah Ten Hag untuk menghapus semua pemain yang bukan dari Manchester United yang ada di directory.  
- Setelah mengetahui nama-nama pemain Manchester United, Ten Hag perlu untuk mengkategorikan pemain tersebut sesuai dengan posisi mereka dalam waktu bersamaan dengan 4 proses yang berbeda. Untuk kategori folder akan menjadi 4 yaitu Kiper, Bek, Gelandang, dan Penyerang.
- Setelah mengkategorikan anggota tim Manchester United, Ten Hag memerlukan Kesebelasan Terbaik untuk menjadi senjata utama MU berdasarkan rating terbaik dengan wajib adanya kiper, bek, gelandang, dan penyerang. (Kiper pasti satu pemain). Untuk output nya akan menjadi Formasi_[jumlah bek]-[jumlah gelandang]-[jumlah penyerang].txt dan akan ditaruh di /home/[users]/
Catatan:
- Format nama file yang akan diunduh dalam zip berupa [nama]_[tim]_[posisi]_[rating].png
- Tidak boleh menggunakan system()
- Tidak boleh memakai function C mkdir() ataupun rename().
- Gunakan exec() dan fork().
- Directory “.” dan “..” tidak termasuk yang akan dihapus.
- Untuk poin d DIWAJIBKAN membuat fungsi bernama buatTim(int, int, int), dengan input 3 value integer dengan urutan bek, gelandang, dan striker.

### Jawab
Sesuai dengan kebutuhan Ten Hag maka langkah langkah yang dapat dilakukan untuk membuat program untuk spawning process dengan fungsi wait karena semua proses harus dilakukan secara sekuensial (contoh proses zip harus selesai terlebih dahulu sebelum melakukan unzip).

1. Melakukan download zip dari link drive yang ada

    Pertama tama kita membuka database dari link drive yang diberikan. Pada link drive tersebut terdapat Id sebagai berikut :

    https://drive.google.com/file/d/1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF/view_

    Dari link tersebut id terletak diantara d/ dan /view. Id tersebut dicopy dan didefinisikan dalam program sehingga nanti ketika download drive maka program dapat mengenali sumber downloadnya :

    ```
    #define FILE_ID "1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF"
    ```
    Kemudian program akan membuka file pada drive sebagai berikut :
    ```
    FILE *fp;
    char *url = "https://drive.google.com/uc?export=download&id=" FILE_ID;
    char outfilename[] = "file.zip";
    ```
    Karena Ten Hag tidak mengizinkan penggunaan system() maka untuk mendownload digunakan execv seperti berikut :
    ```
    char *argumen[] = {"wget", "-O", outfilename, url, NULL};
    execv("/usr/bin/wget", argumen);
    ```

2. Melakukan extract “file.zip”

    Sama seperti sebelumnya extract dilakukan dengan execv sebagai berikut :
    ```
    char *argumen[] = {"unzip", outfilename, NULL};
    execv("/usr/bin/unzip", argumen);
    ```

3. Hapus file zip

    Setelah extract selesai langkah selanjutnya adalah menghapus fili.zip yang sebelumnya sudah didownload dengan cara seperti berikut :
    ```
    char *argumen[] = {"rm", outfilename, NULL};
    execv("/bin/rm", argumen);
    ```

4. Menghapus semua pemain yang bukan dari Manchester United yang ada di directory

    Sesuai dengan format nama file yaitu [nama]_[tim]_[posisi]_[rating].png , maka pencarian shell dapat dilakukan menggunakan shell find pada execv sebagai berikut :
    ```
    char *argumen[] = {"sh", "-c", "find /home/valdo/players -type f ! -name '*ManUtd*' -delete", NULL};
    execv("/bin/sh", argumen);
    ```
    - find: Ini adalah perintah untuk mencari file dan direktori.
    - /home/valdo/players: Ini adalah direktori yang akan dicari. find akan mencari file dan direktori secara rekursif dalam direktori ini dan subdirektorinya.
    - -type f: Opsi ini memberi tahu find untuk hanya mencari file biasa (regular file). Ini akan mengabaikan direktori dan jenis file non-regular lainnya.
    - ! -name '*ManUtd*': Ini adalah opsi negasi nama. Ini memberitahu find untuk mengabaikan file yang namanya mengandung string "ManUtd". Tanda seru (!) membalik pencocokan, sehingga file yang tidak mengandung "ManUtd" dalam namanya akan dipilih.
    - -delete: Opsi ini memberitahu find untuk menghapus file yang dipilih. Jika suatu file cocok dengan kriteria yang ditentukan (jenis file adalah file biasa dan nama tidak mengandung "ManUtd"), maka file tersebut akan dihapus.

    Jadi perintah find lengkap akan mencari file biasa dalam direktori /home/valdo/players dan subdirektorinya, mengabaikan file yang namanya mengandung string "ManUtd", dan menghapus file yang tersisa.

5. Mengkategorikan pemain tersebut sesuai dengan posisi mereka (Kiper, Bek, Gelandang dan Penyerang)

    Pada tahap ini terdapat 2 kali proses yaitu membuat direktori kemudian memasukkan file ke direktori masing masing sesuai posisinya. Ada 4 pengkategorian yaitu : Kiper, Bek, Gelandang dan Penyerang. Berikut adalah eksekusi untuk pembuatan directori :
    ```
    char *argumen[] = {"sh", "-c", "mkdir -p /home/valdo/players/Bek && mkdir -p /home/valdo/players/Gelandang && mkdir -p /home/valdo/players/Kiper && mkdir -p /home/valdo/players/Penyerang", NULL};                          
    execv("/bin/sh", argumen);
    ```
    Berikut adalah eksekusi untuk input file pemain ke direktori :
    ```
    char *argumen[] = {"sh", "-c", "cd /home/valdo/players && mv *Bek*.png Bek/ && mv *Gelandang*.png Gelandang/ && mv *Kiper*.png Kiper/ && mv *Penyerang*.png Penyerang/", NULL};
    execv("/bin/sh", argumen);
    ```

6. Mencari kesebelasan Terbaik untuk menjadi senjata utama MU berdasarkan rating terbaik per posisinya dengan menggunakan fungsi yang menerima 3 argumen yaitu jumlah pemain per formasi.

    Pertama pelu dilakukan konversi string ke integer pada argumen main yang nanti akan dipass ke fungsi BuatTim sebagai berikut :
    ```
    int bek, gelandang, penyerang;
    sscanf(argv[1], "%d", &bek);
    sscanf(argv[2], "%d", &gelandang);
    sscanf(argv[3], "%d", &penyerang);
    ```
    Kemudian lakukan passing argumen ke fungsi BuatTim untuk menjalankan fungsi pencarian kesebelasan dari input yang diberikan
    ```
    BuatTim(bek,gelandang,penyerang);
    ```
    Adapun fungsi BuatTim adalah sebagai berikut :
    ```
    void BuatTim(int bek, int gelandang, int penyerang){
        char filename[40];
        sprintf(filename, "/home/valdo/Formasi-%d-%d-%d.txt", bek, gelandang, penyerang);

        pid_t func_pid = fork();
        if (func_pid == -1) {
            perror("fork error");
            exit(1);
        } else if (func_pid == 0) {
            // Child process
            printf("%s",filename);
            char *argumen[] = {"touch", filename, NULL};
            execv("/usr/bin/touch", argumen);
            perror("execv error");
            exit(1);
        } else {
            // Parent process
            int status;
            if (waitpid(func_pid, &status, 0) == -1) {
                perror("waitpid error");
                exit(1);
            }
            if (WIFEXITED(status) && !WEXITSTATUS(status)) {
                // Inputting name to [filename]
                pid_t func_pid2 = fork();
                if (func_pid2 == -1) {
                    perror("fork error");
                    exit(1);
                } else if (func_pid2 == 0) {
                    // Child process
                    printf("%s",filename);
                    char argumenshk[150];
                    sprintf(argumenshk, "cd /home/valdo/players/Kiper && (ls -r1 | sort -t_ -k4 -rn | head -n 1) > %s",filename);
                    char *argumen[] = {"sh", "-c", argumenshk, NULL};
                    execv("/usr/bin/sh", argumen);
                    perror("execv error");
                    exit(1);
                } else {
                    // Parent process
                    int status2;
                    if (waitpid(func_pid2, &status2, 0) == -1) {
                        perror("waitpid error");
                        exit(1);
                    }
                    if (WIFEXITED(status2) && !WEXITSTATUS(status2)){
                        pid_t func_pid3 = fork();
                        if (func_pid3 == -1) {
                            perror("fork error");
                            exit(1);
                        } else if (func_pid3 == 0){
                            // Child process
                            printf("%s",filename);
                            char argumenshb[150];
                            sprintf(argumenshb, "cd /home/valdo/players/Bek && (ls -r1 | sort -t_ -k4 -rn | head -n %d) >> %s", bek, filename);
                            char *argumen[] = {"sh", "-c",argumenshb, NULL};
                            execv("/usr/bin/sh", argumen);
                            perror("execv error");
                            exit(1);
                        } else {
                            // Parent process
                            int status3;
                            if (waitpid(func_pid3, &status3, 0) == -1) {
                                perror("waitpid error");
                                exit(1);
                            }
                            if (WIFEXITED(status3) && !WEXITSTATUS(status3)){
                                pid_t func_pid4 = fork();
                                if (func_pid4 == -1) {
                                    perror("fork error");
                                    exit(1);
                                } else if (func_pid4 == 0){
                                //Child process
                                printf("%s",filename);
                                char argumenshg[150];
                                sprintf(argumenshg, "cd /home/valdo/players/Gelandang && (ls -r1 | sort -t_ -k4 -rn | head -n %d) >> %s", gelandang, filename);
                                char *argumen[] = {"sh", "-c",argumenshg, NULL};
                                execv("/usr/bin/sh", argumen);
                                perror("execv error");
                                exit(1);
                                } else {
                                //Parent process
                                int status4;
                                if (waitpid(func_pid4, &status4, 0) == -1){
                                    perror("waitpid error");
                                    exit(1);
                                }
                                if (WIFEXITED(status4) && !WEXITSTATUS(status4)){
                                    pid_t func_pid5 = fork();
                                    if (func_pid5 == -1) exit(1);
                                    else if (func_pid5 == 0){
                                    //Child process
                                    printf("%s",filename);
                                    char argumenshp[150];
                                    sprintf(argumenshp, "cd /home/valdo/players/Penyerang && (ls -r1 | sort -t_ -k4 -rn | head -n %d) >> %s", penyerang, filename);
                                    char *argumen[] = {"sh", "-c",argumenshp, NULL};
                                    execv("/usr/bin/sh", argumen);
                                    perror("execv error");
                                    exit(1);
                                    } else {
                                    //Parent process
                                    int status5;
                                    waitpid(func_pid5, &status5, 0);
                                    printf("All process done, bye bye sisop\n");
                                    }
                                }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    ```
        
    Pertama fungsi akan membuat sebuah string filename yang berisi path dan nama file untuk menyimpan data pemain di dalam tim. String ini dibuat menggunakan fungsi sprintf dengan format "/home/valdo/Formasi-%d-%d-%d.txt" yang masing-masing diisi dengan nilai bek, gelandang, dan penyerang.

    Dilanjutkan dengan membuat sebuah child process menggunakan fork() dan menyimpan hasil return-nya pada variabel func_pid. Jika terdapat error pada saat melakukan fork, maka program akan menampilkan pesan error dan keluar dari program. Jika func_pid bernilai 0, maka itu menunjukkan bahwa saat ini program sedang berjalan pada child process.

    Pada child process, program akan mengeksekusi perintah touch dengan menggunakan fungsi execv. Perintah ini berfungsi untuk membuat file baru dengan nama filename. Jika terdapat error pada saat melakukan execv, maka program akan menampilkan pesan error dan keluar dari program.

    Jika func_pid bukan bernilai 0 (yaitu saat ini program sedang berjalan pada parent process), maka program akan menunggu child process selesai dieksekusi menggunakan waitpid(func_pid, &status, 0). Jika terdapat error pada saat menunggu child process, maka program akan menampilkan pesan error dan keluar dari program.

    Setelah child process selesai dieksekusi, program akan mengecek apakah child process tersebut berjalan dengan sukses menggunakan WIFEXITED(status) dan WEXITSTATUS(status). Jika berhasil, program akan membuat child process baru untuk menginputkan nama-nama pemain di dalam tim berdasarkan posisi mereka.

    Child process baru tersebut akan menggunakan perintah ls, sort, dan head untuk mencari file-file pemain di dalam direktori /home/valdo/players/ yang sesuai dengan posisi pemain (bek, gelandang, dan penyerang). Nama file-file tersebut kemudian disimpan ke dalam file filename menggunakan operator >>.

    Proses diulangi untuk setiap posisi pemain, yaitu untuk bek, gelandang, dan penyerang.

7. Menggabungkan semua proses sehingga terjadi secara sekuensial

    Untuk menjalankan program secara sequensial maka tiap proses dilakukan wait child selesai dan kemusian parent akan spawn child kembali hingga akhir proses. Berikut adalah hasil lengkap pencarian kesebelasan Ten hag :
    ```
    #include <sys/types.h>
    #include <sys/stat.h>
    #include <stdio.h>
    #include <stdlib.h>
    #include <fcntl.h>
    #include <errno.h>
    #include <unistd.h>
    #include <syslog.h>
    #include <string.h>
    #include <time.h>
    #include <sys/wait.h>

    #define FILE_ID "1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF"

    int check_number(char array[]){
        for(int i=0; i<strlen(array); i++){
            if(array[0]<'0' || array[0]>'9') return 0;
            return 1;
        }
    }

    void BuatTim(int bek, int gelandang, int penyerang){
        char filename[40];
        sprintf(filename, "/home/valdo/Formasi-%d-%d-%d.txt", bek, gelandang, penyerang);

        pid_t func_pid = fork();
        if (func_pid == -1) {
            perror("fork error");
            exit(1);
        } else if (func_pid == 0) {
            // Child process
            printf("%s",filename);
            char *argumen[] = {"touch", filename, NULL};
            execv("/usr/bin/touch", argumen);
            perror("execv error");
            exit(1);
        } else {
            // Parent process
            int status;
            if (waitpid(func_pid, &status, 0) == -1) {
                perror("waitpid error");
                exit(1);
            }
            if (WIFEXITED(status) && !WEXITSTATUS(status)) {
                // Inputting name to [filename]
                pid_t func_pid2 = fork();
                if (func_pid2 == -1) {
                    perror("fork error");
                    exit(1);
                } else if (func_pid2 == 0) {
                    // Child process
                    printf("%s",filename);
                    char argumenshk[150];
                    sprintf(argumenshk, "cd /home/valdo/players/Kiper && (ls -r1 | sort -t_ -k4 -rn | head -n 1) > %s",filename);
                    char *argumen[] = {"sh", "-c", argumenshk, NULL};
                    execv("/usr/bin/sh", argumen);
                    perror("execv error");
                    exit(1);
                } else {
                    // Parent process
                    int status2;
                    if (waitpid(func_pid2, &status2, 0) == -1) {
                        perror("waitpid error");
                        exit(1);
                    }
                    if (WIFEXITED(status2) && !WEXITSTATUS(status2)){
                        pid_t func_pid3 = fork();
                        if (func_pid3 == -1) {
                            perror("fork error");
                            exit(1);
                        } else if (func_pid3 == 0){
                            // Child process
                            printf("%s",filename);
                            char argumenshb[150];
                            sprintf(argumenshb, "cd /home/valdo/players/Bek && (ls -r1 | sort -t_ -k4 -rn | head -n %d) >> %s", bek, filename);
                            char *argumen[] = {"sh", "-c",argumenshb, NULL};
                            execv("/usr/bin/sh", argumen);
                            perror("execv error");
                            exit(1);
                        } else {
                            // Parent process
                            int status3;
                            if (waitpid(func_pid3, &status3, 0) == -1) {
                                perror("waitpid error");
                                exit(1);
                            }
                            if (WIFEXITED(status3) && !WEXITSTATUS(status3)){
                                pid_t func_pid4 = fork();
                                if (func_pid4 == -1) {
                                    perror("fork error");
                                    exit(1);
                                } else if (func_pid4 == 0){
                                //Child process
                                printf("%s",filename);
                                char argumenshg[150];
                                sprintf(argumenshg, "cd /home/valdo/players/Gelandang && (ls -r1 | sort -t_ -k4 -rn | head -n %d) >> %s", gelandang, filename);
                                char *argumen[] = {"sh", "-c",argumenshg, NULL};
                                execv("/usr/bin/sh", argumen);
                                perror("execv error");
                                exit(1);
                                } else {
                                //Parent process
                                int status4;
                                if (waitpid(func_pid4, &status4, 0) == -1){
                                    perror("waitpid error");
                                    exit(1);
                                }
                                if (WIFEXITED(status4) && !WEXITSTATUS(status4)){
                                    pid_t func_pid5 = fork();
                                    if (func_pid5 == -1) exit(1);
                                    else if (func_pid5 == 0){
                                    //Child process
                                    printf("%s",filename);
                                    char argumenshp[150];
                                    sprintf(argumenshp, "cd /home/valdo/players/Penyerang && (ls -r1 | sort -t_ -k4 -rn | head -n %d) >> %s", penyerang, filename);
                                    char *argumen[] = {"sh", "-c",argumenshp, NULL};
                                    execv("/usr/bin/sh", argumen);
                                    perror("execv error");
                                    exit(1);
                                    } else {
                                    //Parent process
                                    int status5;
                                    waitpid(func_pid5, &status5, 0);
                                    printf("All process done, bye bye sisop\n");
                                    }
                                }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    int main(int argc, char** argv) {
    if (argc==4){
        for(int i=1; i<4; i++){
            if(!check_number(argv[i])){
            printf("argument %d  is not valid.\n", i);
            exit(EXIT_FAILURE);
            }
        }

        FILE *fp;
        char *url = "https://drive.google.com/uc?export=download&id=" FILE_ID;
        char outfilename[] = "file.zip";

        // Download the file using fork() and execv()
        pid_t pid = fork();
        if (pid == -1) return 1;
        else if (pid == 0) {
        // Child process
        printf("downloading...\n");
        char *argumen[] = {"wget", "-O", outfilename, url, NULL};
        execv("/usr/bin/wget", argumen);
        exit(1);
        } else {
        // Parent process
        int status;
        waitpid(pid, &status, 0);
        if (WIFEXITED(status) && !WEXITSTATUS(status)) {
            // Download succeeded, unzip the file using fork() and execv()
            pid_t pid2 = fork();
            if (pid2 == -1) return 1;
            else if (pid2 == 0) {
            // Child process
            printf("unzipping...\n");
            char *argumen[] = {"unzip", outfilename, NULL};
            execv("/usr/bin/unzip", argumen);
            exit(1);
            } else {
            // Parent process
            int status2;
            waitpid(pid2, &status2, 0);
            if (WIFEXITED(status2) && !WEXITSTATUS(status2)) {
                // Unzip succeeded, remove the zip file using fork() and execv()
                pid_t pid3 = fork();
                if (pid3 == -1) return 1;
                else if (pid3 == 0) {
                // Child process
                printf("removing...\n");
                char *argumen[] = {"rm", outfilename, NULL};
                execv("/bin/rm", argumen);
                exit(1);
                } else {
                // Parent process
                int status3;
                waitpid(pid3, &status3, 0);
                if (WIFEXITED(status3) && !WEXITSTATUS(status3)){
                    pid_t pid4 = fork();
                    if (pid4 == -1) return 1;
                    else if (pid4 == 0) {
                        //Child pocess
                        printf("searching(ManUtd)...\n");
                        char *argumen[] = {"sh", "-c", "find /home/valdo/players -type f ! -name '*ManUtd*' -delete", NULL};
                        execv("/bin/sh", argumen);
                        exit(1);
                    } else {
                        //Parent process
                        int status4;
                        waitpid(pid4, &status4, 0);
                        if (WIFEXITED(status4) && !WEXITSTATUS(status4)){
                            pid_t pid5 = fork();
                            if (pid5 == -1) return 1;
                            else if (pid5 == 0){
                                //Child pocess
                                printf("making folder...\n");
                                char *argumen[] = {"sh", "-c", "mkdir -p /home/valdo/players/Bek && mkdir -p /home/valdo/players/Gelandang && mkdir -p /home/valdo/players/Kiper && mkdir -p /home/valdo/players/Penyerang", NULL};                          
                                execv("/bin/sh", argumen);
                                exit(1);
                            }else{
                                //Parent process
                                int status5;
                                waitpid(pid5, &status5, 0);
                                if (WIFEXITED(status5) && !WEXITSTATUS(status5)){
                                    pid_t pid6 = fork();
                                    if (pid6 == -1) return 1;
                                    else if (pid6 == 0){
                                        //Child pocess
                                        printf("moving file...\n");
                                        char *argumen[] = {"sh", "-c", "cd /home/valdo/players && mv *Bek*.png Bek/ && mv *Gelandang*.png Gelandang/ && mv *Kiper*.png Kiper/ && mv *Penyerang*.png Penyerang/", NULL};
                                        execv("/bin/sh", argumen);
                                        exit(1);
                                    } else {
                                        //Parent process
                                        int status6;
                                        waitpid(pid6, &status6, 0);
                                        if (WIFEXITED(status6) && !WEXITSTATUS(status6)){
                                            pid_t pid7 = fork();
                                            if (pid7 == -1) return 1;
                                            else if (pid7 == 0){
                                                //Child pocess
                                                printf("running function...\n");
                                                int bek, gelandang, penyerang;
                                                sscanf(argv[1], "%d", &bek);
                                                sscanf(argv[2], "%d", &gelandang);
                                                sscanf(argv[3], "%d", &penyerang);
                                                BuatTim(bek,gelandang,penyerang);
                                            } else {
                                                //Parent process
                                                int status7;
                                                waitpid(pid7, &status7, 0);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                }
            }
            }
        } 
        }
    } else {
        printf("argument is not valid.\n");
        exit(EXIT_FAILURE);
    }
    return 0;
    }
    ```

### Output Program
1. Setelah proses download

    ![ ](./img/3.Gambar_1.png)

    Hasil sukses download dapat dilihat pada directory path apakah sudah ada file.zip seperti yang dapat dilihat di gambar diatas.

2. Setelah proses unzip

    ![ ](./img/3.Gambar_2.png)

    Proses tersebut selesai saat pada directory path terdapat directory players

    ![ ](./img/3.Gambar_3.png)

    Dalam directory path terdapat 2 file yaitu directory players dan file.zip yang selanjutnya perlu untuk dihapus.

3. Setelah proses remove zip

    ![ ](./img/3.Gambar_4.png)

    Setelah proses remove zip selesai maka pada directory hanya akan ada directory players.

4. Setelah proses hapus pemain selain ManUtd

    ![ ](./img/3.Gambar_5.png)

    Semua file yang tidak mengandung "ManUtd" akan terhapus dari directory players

5. Setelah proses Pengelompokan posisi

    ![ ](./img/3.Gambar_6.png)

    Berikut diatas adalah hasil proses pengelompokan semua pemain Manutd sesuai posisinya.

6. Hasil Formasi

    ![ ](./img/3.Gambar_7.png)

    Hasil diatas adalah formasi kesebelasan dengan rating terbaik per posisi dengan jumlah 1 kiper dan config semula.

### Kendala Pengerjaan
Program sangat panjang sehingga sulit ketika debugging terutama karena banyaknya nama variabel yang berbeda.

## 🧸 Nomor 4 
### Soal
Banabil adalah seorang mahasiswa yang rajin kuliah dan suka belajar. Namun naasnya Banabil salah mencari teman, dia diajak ke toko mainan oleh teman-temannya dan teracuni untuk membeli banyak sekali mainan dan kebingungan memilih mainan mana yang harus dibeli. Hal tersebut menyebabkan Banabil kehilangan fokus dalam pengerjaan tugas-tugas yang diberikan oleh dosen nya. Untuk mengembalikan fokusnya, Banabil harus melatih diri sendiri dalam membuat program untuk menjalankan script bash yang menyerupai crontab dan menggunakan bahasa C karena baru dipelajari olehnya.
Untuk menambah tantangan agar membuatnya semakin terfokus, Banabil membuat beberapa ketentuan custom yang harus dia ikuti sendiri. Ketentuan tersebut berupa:
- Banabil tidak ingin menggunakan fungsi system(), karena terlalu mudah.
- Dalam pelatihan fokus time managementnya, Banabil harus bisa membuat program yang dapat menerima argumen berupa Jam (0-23), Menit (0-59), Detik (0-59), Tanda asterisk [ * ] (value bebas), serta path file .sh.
- Dalam pelatihan fokus untuk ketepatan pilihannya, Banabil ingin programnya dapat mengeluarkan pesan “error” apabila argumen yang diterima program tidak sesuai. Pesan error dapat dibentuk sesuka hati oleh pembuat program. terserah bagaimana, yang penting tulisan error.
- Terakhir, dalam pelatihan kesempurnaan fokusnya, Banabil ingin program ini berjalan dalam background dan hanya menerima satu config cron.
- Bonus poin apabila CPU state minimum.
Contoh untuk run: /program \* 44 5 /home/Banabil/programcron.sh

### Jawab

Sesuai dengan permintaan Banabil maka langkah langkah yang dapat dilakukan untuk membuat program Banabil adalah sebagai berikut :
1. Membentuk struktur daemon sehingga program dapat berjalan di background

    Di bawah ini adalah kode hasil gabungan dari langkah-langkah pembuatan daemon dari modul 2 praktikum SISOP (template Daemon):
    ```
    #include <sys/types.h>
    #include <sys/stat.h>
    #include <stdio.h>
    #include <stdlib.h>
    #include <fcntl.h>
    #include <errno.h>
    #include <unistd.h>
    #include <syslog.h>
    #include <string.h>

    int main() {
    pid_t pid, sid;        // Variabel untuk menyimpan PID

    pid = fork();     // Menyimpan PID dari Child Process

    /* Keluar saat fork gagal
    * (nilai variabel pid < 0) */
    if (pid < 0) {
        exit(EXIT_FAILURE);
    }

    /* Keluar saat fork berhasil
    * (nilai variabel pid adalah PID dari child process) */
    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }

    if ((chdir("/")) < 0) {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    while (1) {
        // Tulis program kalian di sini

        sleep(1);
    }
    }
    ```
2. Menambahkan argumen mula mula untuk menjalankan fungsi main dan melakukan pengecekkan pada argumen yang dimasukkan

    Kemudian agar program dapat menerima argumen config cron (detik,menit,jam dan path) maka pada bagian main ditambahkan parameter sebagai berikut :
    ```c
    int main(int argc, char** argv)
    ```
    Penambahan parameter tersebut dimaksudkan agar program dapat menerima beberapa argumen sebelum dirun. Tidak cukup hanya dengan menerima argumen program Banabil juga harus mampu untuk memastikan program dapat berjalan dengan baik melalui proses error checking. Pemeriksaan error meliputi :
    - Jumlah argumen yang dimasukkan

        pengecekkan jumlah argumen dapat dilakukan dengan percabangan sebagai berikut :
        ```
        if(argc==5){
            //jalankan pengecekkan yang lain
        } else {
            printf("argument is not valid.\n"); //pesan error
        }
        ```
    - Isian hanya angka atau karakter '*' untuk argumen waktu

        Setelah dipastikan jumlah argumen sesuai maka akan dicek argumen pertama, kedua dan ketiga menggunakan fungsi cek angka dan cek bintang. Tujuan dari kedua fungsi ini adalah melakukan check apakah argumen sudah benar yaitu angka atau bintang. Berikut adalah fungsi pengecekkan tersebut :
        ```
        int cek_bintang(char array[]){
            if(array[0] == '*') return 1;
            return 0;
        }
        int check_number(char array[]){
            for(int i=0; i<strlen(array); i++){
                if(array[0]<'0' || array[0]>'9') return 0;
                return 1;
            }
        }
        ```
        Setelah dibuat fungsi maka selanjutnya adalah melakukan iterasi pada argumen inputan dengan memanfaatkan for loop. Setiap argumen akan dicek apakah dia berisi angka kemudian jika tidak dicek apakah dia karakter '*' jika tidak maka ada kesalahan pada argumen sehingga tampilkan pesan error. Berikut adalah implementasinya :
        ```
        int waktu[4];
        for(int i=1; i<4; i++){
            if(check_number(argv[i])){
                sscanf(argv[i], "%d", &waktu[i]);
            } else if(cek_bintang(argv[i])){
                waktu[i] = -1;
            } else {
                printf("argument %d  is not valid.\n", i);
                exit(EXIT_FAILURE);
            }
        }
        ```
        Dalam program tersebut ditambahkan sebuah array waktu untuk menyimpan nilai argumen yang semula string menjadi integer dengan memanfaatkan fungsi sscanf() sedangkan untuk karakter '*' akan disimpan dengan identifier -1.
    - Memastikan interval waktu yang dimasukkan adalah sesuai dengan format waktu yaitu : Jam (0-23), Menit (0-59), dan Detik (0-59)

        Untuk mengecek waktu perlu diperhatikan bahwa jam selalu kurang dari 24, menit selalu kurang dari 60 dan detik juga selalu kurang dari 60, sehingga implementasi pengecekkan nilai integer jam adalah sebagai berikut :
        ```
        if(waktu[1] > 23 || waktu[2] > 59 || waktu[3] > 59){
            printf("argument is not valid.\n");
            exit(EXIT_FAILURE);
        }
        ```
3. Menjalankan program seperti Cron yang sesuai config program saat dirun dengan waktu local

    Cron dalam program Banabil membandingkan config semula dengan waktu local maka pertama tama perlu untuk mengambil value local time saat ini sebagai berikut :
    ```
    time_t now;
    time(&now);
    struct tm *local = localtime(&now);
    int skrg1, skrg2, skrg3;

    skrg1 = local->tm_hour;
    skrg2 = local->tm_min;
    skrg3 = local->tm_sec;
    ```
    Hasil pembacaan local time akan diletakkan pada 3 variabel sesuai namanya sebagai berikut : skrg1(jam), skrg2(menit), skrg3(detik). Selanjutnya adalah membandingkan value tersebut dengan config semula dapat dilakukan sebagai berikut :
    ```
    if((skrg1 == waktu[3] || waktu[3] == -1) && (skrg2 == waktu[2] || waktu[2] == -1) && (skrg3 == waktu[1] || waktu[1] == -1)){
        pid_t child_id;
        child_id = fork();
                    
        if(child_id==0){
            char *arg[] = {"bash", argv[4], NULL};
            execv("/bin/bash", arg);
        }
    }
    ```
    Dalam implementasi tersebut jika value waktu sudah sesuai maka pada waktu tersebut proses daemon akan membuat child process yang melakukan bash sebuah file shell sesuai argumen 4 config.

4. Source Code

    Berikut adalah gabungan source code untuk nomor 4 :
    ```
    #include <sys/types.h>
    #include <sys/stat.h>
    #include <stdio.h>
    #include <stdlib.h>
    #include <fcntl.h>
    #include <errno.h>
    #include <unistd.h>
    #include <syslog.h>
    #include <string.h>
    #include <time.h>

    int cek_bintang(char array[]){
        if(array[0] == '*') return 1;
        return 0;
    }
    int check_number(char array[]){
        for(int i=0; i<strlen(array); i++){
            if(array[0]<'0' || array[0]>'9') return 0;
            return 1;
        }
    }
    
    int main(int argc, char** argv){
        if(argc==5){
        int waktu[4];
        for(int i=1; i<4; i++){
            if(check_number(argv[i])){
                sscanf(argv[i], "%d", &waktu[i]);
            } else if(cek_bintang(argv[i])){
                waktu[i] = -1;
            } else {
                printf("argument %d  is not valid.\n", i);
                exit(EXIT_FAILURE);
            }
        }

        if(waktu[1] > 23 || waktu[2] > 59 || waktu[3] > 59){
            printf("argument is not valid.\n");
            exit(EXIT_FAILURE);
        }

        pid_t pid, sid;
        pid = fork();

        if (pid < 0) exit(EXIT_FAILURE);
        if (pid > 0) exit(EXIT_SUCCESS);

        umask(0);

        sid = setsid();
        if (sid < 0) exit(EXIT_FAILURE);

        close(STDIN_FILENO);
        close(STDOUT_FILENO);
        close(STDERR_FILENO);

        while(1){
            time_t now;
            time(&now);
            struct tm *local = localtime(&now);
            int skrg1, skrg2, skrg3;

            skrg1 = local->tm_hour;
            skrg2 = local->tm_min;
            skrg3 = local->tm_sec;

            if((skrg1 == waktu[3] || waktu[3] == -1) && (skrg2 == waktu[2] || waktu[2] == -1) && (skrg3 == waktu[1] || waktu[1] == -1)){
            pid_t child_id;
            child_id = fork();
                    
            if(child_id==0){
                char *arg[] = {"bash", argv[4], NULL};
                execv("/bin/bash", arg);
            }
            } 
            sleep(1);
        }
        } else {
            printf("argument is not valid.\n");
        }
    }
    ```
### Output Program

Untuk run program maka seperti instruksi saat melakukan running format config yang harus dimasukkan adalah sebagai berikut :
```
./mainan '[Jam]' '[Menit]' '[Detik]' '[Path file sh]'
```
Berikut adalah hasil pengecekkan saat argumen yang dimasukkan tidak sesuai :
![ ](./img/4.Gambar_1.png)

Jika argumen sudah benar maka untuk melihat proses daemon yang sedang berjalan dapat digunakan **ps aux | grep mainan** sebagai berikut :
![ ](./img/4.Gambar_2.png)

Dalam kasus saat ini *home/valdo/ProgramCron* adalah program untuk menuliskan waktu mainan sukses menjalankan programnya. Berikut adalah code ProgramCron.sh :
```
#!/bin/bash

# Menambahkan timestamp pada pesan
pesan="pada $(date '+%H:%M:%S') program berjalan"

# Menuliskan pesan ke file hasil.txt
echo $pesan >> hasil.txt
```

Berikut adalah hasil pada hasil.txt dengan config * * * home/valdo/ProgramCron.sh :
![ ](./img/4.Gambar_3.png)

Untuk melakukan check CPU dapat digunakan command htop di terminal. Berikut adalah efisiensi program mainan :
![ ](./img/4.Gambar_4.png)
Program tersebut sudah efisien dengan kinerja CPU hanya 0.0

### Kendala Pengerjaan
Saat awal mula pengerjaan program kami tidak menemukan error yang cukup signifikan yaitu karena tidak terbiasa menerima argumen dari fungsi main. Pada awalnya kami mengangap argumen langsung sebagai integer yang mana sebenarnya harus diconvert dahulu dari string.
