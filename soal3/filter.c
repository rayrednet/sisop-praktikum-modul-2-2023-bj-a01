#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <time.h>
#include <sys/wait.h>

#define FILE_ID "1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF"

int check_number(char array[]){
    for(int i=0; i<strlen(array); i++){
        if(array[0]<'0' || array[0]>'9') return 0;
        return 1;
    }
}

void BuatTim(int bek, int gelandang, int penyerang){
    char filename[40];
    sprintf(filename, "/home/valdo/Formasi-%d-%d-%d.txt", bek, gelandang, penyerang);

    pid_t func_pid = fork();
    if (func_pid == -1) {
        perror("fork error");
        exit(1);
    } else if (func_pid == 0) {
        // Child process
        printf("%s",filename);
        char *argumen[] = {"touch", filename, NULL};
        execv("/usr/bin/touch", argumen);
        perror("execv error");
        exit(1);
    } else {
        // Parent process
        int status;
        if (waitpid(func_pid, &status, 0) == -1) {
            perror("waitpid error");
            exit(1);
        }
        if (WIFEXITED(status) && !WEXITSTATUS(status)) {
            // Inputting name to [filename]
            pid_t func_pid2 = fork();
            if (func_pid2 == -1) {
                perror("fork error");
                exit(1);
            } else if (func_pid2 == 0) {
                // Child process
                printf("%s",filename);
                char argumenshk[150];
                sprintf(argumenshk, "cd /home/valdo/players/Kiper && (ls -r1 | sort -t_ -k4 -rn | head -n 1) > %s",filename);
                char *argumen[] = {"sh", "-c", argumenshk, NULL};
                execv("/usr/bin/sh", argumen);
                perror("execv error");
                exit(1);
            } else {
                // Parent process
                int status2;
                if (waitpid(func_pid2, &status2, 0) == -1) {
                    perror("waitpid error");
                    exit(1);
                }
                if (WIFEXITED(status2) && !WEXITSTATUS(status2)){
                    pid_t func_pid3 = fork();
                    if (func_pid3 == -1) {
                        perror("fork error");
                        exit(1);
                    } else if (func_pid3 == 0){
                        // Child process
                        printf("%s",filename);
                        char argumenshb[150];
                        sprintf(argumenshb, "cd /home/valdo/players/Bek && (ls -r1 | sort -t_ -k4 -rn | head -n %d) >> %s", bek, filename);
                        char *argumen[] = {"sh", "-c",argumenshb, NULL};
                        execv("/usr/bin/sh", argumen);
                        perror("execv error");
                        exit(1);
                    } else {
                        // Parent process
                        int status3;
                        if (waitpid(func_pid3, &status3, 0) == -1) {
                            perror("waitpid error");
                            exit(1);
                        }
                        if (WIFEXITED(status3) && !WEXITSTATUS(status3)){
                            pid_t func_pid4 = fork();
                            if (func_pid4 == -1) {
                                perror("fork error");
                                exit(1);
                            } else if (func_pid4 == 0){
                              //Child process
                              printf("%s",filename);
                              char argumenshg[150];
                              sprintf(argumenshg, "cd /home/valdo/players/Gelandang && (ls -r1 | sort -t_ -k4 -rn | head -n %d) >> %s", gelandang, filename);
                              char *argumen[] = {"sh", "-c",argumenshg, NULL};
                              execv("/usr/bin/sh", argumen);
                              perror("execv error");
                              exit(1);
                            } else {
                              //Parent process
                              int status4;
                              if (waitpid(func_pid4, &status4, 0) == -1){
                                  perror("waitpid error");
                                  exit(1);
                              }
                              if (WIFEXITED(status4) && !WEXITSTATUS(status4)){
                                pid_t func_pid5 = fork();
                                if (func_pid5 == -1) exit(1);
                                else if (func_pid5 == 0){
                                  //Child process
                                  printf("%s",filename);
                                  char argumenshp[150];
                                  sprintf(argumenshp, "cd /home/valdo/players/Penyerang && (ls -r1 | sort -t_ -k4 -rn | head -n %d) >> %s", penyerang, filename);
                                  char *argumen[] = {"sh", "-c",argumenshp, NULL};
                                  execv("/usr/bin/sh", argumen);
                                  perror("execv error");
                                  exit(1);
                                } else {
                                  //Parent process
                                  int status5;
                                  waitpid(func_pid5, &status5, 0);
                                  printf("All process done, bye bye sisop\n");
                                }
                              }
                            }
                        }
                      }
                  }
              }
          }
      }
}

int main(int argc, char** argv) {
  if (argc==4){
    for(int i=1; i<4; i++){
        if(!check_number(argv[i])){
          printf("argument %d  is not valid.\n", i);
          exit(EXIT_FAILURE);
        }
    }

    FILE *fp;
    char *url = "https://drive.google.com/uc?export=download&id=" FILE_ID;
    char outfilename[] = "file.zip";

    // Download the file using fork() and execv()
    pid_t pid = fork();
    if (pid == -1) return 1;
    else if (pid == 0) {
      // Child process
      printf("downloading...\n");
      char *argumen[] = {"wget", "-O", outfilename, url, NULL};
      execv("/usr/bin/wget", argumen);
      exit(1);
    } else {
      // Parent process
      int status;
      waitpid(pid, &status, 0);
      if (WIFEXITED(status) && !WEXITSTATUS(status)) {
        // Download succeeded, unzip the file using fork() and execv()
        pid_t pid2 = fork();
        if (pid2 == -1) return 1;
        else if (pid2 == 0) {
          // Child process
          printf("unzipping...\n");
          char *argumen[] = {"unzip", outfilename, NULL};
          execv("/usr/bin/unzip", argumen);
          exit(1);
        } else {
          // Parent process
          int status2;
          waitpid(pid2, &status2, 0);
          if (WIFEXITED(status2) && !WEXITSTATUS(status2)) {
            // Unzip succeeded, remove the zip file using fork() and execv()
            pid_t pid3 = fork();
            if (pid3 == -1) return 1;
            else if (pid3 == 0) {
              // Child process
              printf("removing...\n");
              char *argumen[] = {"rm", outfilename, NULL};
              execv("/bin/rm", argumen);
              exit(1);
            } else {
              // Parent process
              int status3;
              waitpid(pid3, &status3, 0);
              if (WIFEXITED(status3) && !WEXITSTATUS(status3)){
                  pid_t pid4 = fork();
                  if (pid4 == -1) return 1;
                  else if (pid4 == 0) {
                      //Child pocess
                      printf("searching(ManUtd)...\n");
                      char *argumen[] = {"sh", "-c", "find /home/valdo/players -type f ! -name '*ManUtd*' -delete", NULL};
                      execv("/bin/sh", argumen);
                      exit(1);
                  } else {
                      //Parent process
                      int status4;
                      waitpid(pid4, &status4, 0);
                      if (WIFEXITED(status4) && !WEXITSTATUS(status4)){
                          pid_t pid5 = fork();
                          if (pid5 == -1) return 1;
                          else if (pid5 == 0){
                              //Child pocess
                              printf("making folder...\n");
                              char *argumen[] = {"sh", "-c", "mkdir -p /home/valdo/players/Bek && mkdir -p /home/valdo/players/Gelandang && mkdir -p /home/valdo/players/Kiper && mkdir -p /home/valdo/players/Penyerang", NULL};                          
                              execv("/bin/sh", argumen);
                              exit(1);
                          }else{
                              //Parent process
                              int status5;
                              waitpid(pid5, &status5, 0);
                              if (WIFEXITED(status5) && !WEXITSTATUS(status5)){
                                  pid_t pid6 = fork();
                                  if (pid6 == -1) return 1;
                                  else if (pid6 == 0){
                                      //Child pocess
                                      printf("moving file...\n");
                                      char *argumen[] = {"sh", "-c", "cd /home/valdo/players && mv *Bek*.png Bek/ && mv *Gelandang*.png Gelandang/ && mv *Kiper*.png Kiper/ && mv *Penyerang*.png Penyerang/", NULL};
                                      execv("/bin/sh", argumen);
                                      exit(1);
                                  } else {
                                      //Parent process
                                      int status6;
                                      waitpid(pid6, &status6, 0);
                                      if (WIFEXITED(status6) && !WEXITSTATUS(status6)){
                                          pid_t pid7 = fork();
                                          if (pid7 == -1) return 1;
                                          else if (pid7 == 0){
                                              //Child pocess
                                              printf("running function...\n");
                                              int bek, gelandang, penyerang;
                                              sscanf(argv[1], "%d", &bek);
                                              sscanf(argv[2], "%d", &gelandang);
                                              sscanf(argv[3], "%d", &penyerang);
                                              BuatTim(bek,gelandang,penyerang);
                                          } else {
                                              //Parent process
                                              int status7;
                                              waitpid(pid7, &status7, 0);
                                          }
                                      }
                                  }
                              }
                          }
                      }
                  }
              }
            }
          }
        }
      } 
    }
  } else {
    printf("argument is not valid.\n");
    exit(EXIT_FAILURE);
  }
  return 0;
}
