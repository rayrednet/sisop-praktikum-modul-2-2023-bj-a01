#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <time.h>
#include <sys/wait.h>
#include <string.h>
#include <sys/stat.h>
#include <signal.h>

char dirr[20];
int stop = 0;

void download_img(){
    char buff[100];

    //digunakan untuk penamaan img
    time_t now = time(NULL);
    strftime(buff, 20, "%Y-%m-%d_%H:%M:%S", localtime(&now));

    //penamaan img
    char name_img[150];
    sprintf(name_img, "%s.jpeg", buff);

    //mengambil seconds
    time_t seconds;
    seconds = time(NULL);

    //menetapkan ukuran
    int ukuran = (seconds % 1000) + 50;

    //mengambil link url dengan ukuran yang telah disesuaikan
    char link_img[100];
    sprintf(link_img, "https://picsum.photos/%d", ukuran);

    if(fork() == 0){
        //akan download gambar
        char *gmbr[5] = {"wget", "-qO", name_img, link_img, NULL};
        execv("/usr/bin/wget", gmbr);
    }
}


void make_dir(){

    // membuat directory baru 
    char *argvp[3] = {"mkdir", dirr, NULL};
    execvp("mkdir", argvp);
}


int main(int argc, char *argv[]){

    if(argc != 2){

        puts("Argument is not valid");
        exit(0);
    }

    pid_t pid, sid;

    //membuat parent id
    pid = fork();

    if(pid < 0){
        exit(EXIT_FAILURE);
    }

    if(pid > 0){
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();

    if(sid < 0){
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    //handle untuk inputan ./lukisan -a
    if (strcmp(argv[1], "-a") == 0){

        FILE *fPtr = NULL;
        fPtr = fopen("killer.c", "w");

        fprintf(fPtr, "#include <stdio.h>\n");
        fprintf(fPtr, "#include <stdlib.h>\n");
        fprintf(fPtr, "#include <unistd.h>\n");
        fprintf(fPtr, "#include <sys/wait.h>\n\n");
        fprintf(fPtr, "int main(int argc, char *argv[]) {\n");
        fprintf(fPtr, "char* args[] = {\"pkill\", \"lukisan\", NULL};\n");
        fprintf(fPtr, "pid_t pid = fork();\n\n");
        fprintf(fPtr, "if (pid == 0) {\n");
        fprintf(fPtr, "\t// Child process\n");
        fprintf(fPtr, "\texecvp(\"pkill\", args);\n");
        fprintf(fPtr, "} else if (pid > 0) {\n");
        fprintf(fPtr, "\t// Parent process\n");
        fprintf(fPtr, "\twait(NULL);\n");
        fprintf(fPtr, "\tremove(\"killer.c\");\n");
        fprintf(fPtr, "\tremove(\"killer\");\n");
        fprintf(fPtr, "} else {\n");
        fprintf(fPtr, "\t// Forking error\n");
        fprintf(fPtr, "\tprintf(\"Failed to fork process\\n\");\n");
        fprintf(fPtr, "}\n");
        fprintf(fPtr, "return 0; \n}");

        fclose(fPtr);

        pid_t fid = fork();

        if (fid == -1) {
            printf("Failed to fork process\n");
        } 
        
        // akan membuat killer.c telah dicompile
        if (fid == 0) {
            // Child process
            char* args[] = {"gcc", "killer.c", "-o", "killer", NULL};
            execvp("gcc", args);
            printf("Failed to compile killer.c\n");
        } else {
            // Parent process
            wait(NULL);
        }
    }

    //handle untuk inputan ./lukisan -b
    else if (strcmp(argv[1], "-b") == 0){

        FILE *fPtr = NULL;
        fPtr = fopen("killer.c", "w");

        int id = getpid();

        fprintf(fPtr, "#include <stdio.h>\n");
        fprintf(fPtr, "#include <stdlib.h>\n");
        fprintf(fPtr, "#include <unistd.h>\n");
        fprintf(fPtr, "#include <signal.h>\n");
        fprintf(fPtr, "#include <sys/wait.h>\n\n");
        fprintf(fPtr, "int main() {\n");
        fprintf(fPtr, "pid_t id2 = %d;\n", id);
        fprintf(fPtr, "pid_t pid = fork();\n\n");
        fprintf(fPtr, "if (pid == 0) {\n");
        fprintf(fPtr, "\t// Child process\n");
        fprintf(fPtr, "\tkill(id2, SIGTERM);\n");
        fprintf(fPtr, "} else if (pid > 0) {\n");
        fprintf(fPtr, "\t// Parent process\n");
        fprintf(fPtr, "\twait(NULL);\n");
        fprintf(fPtr, "\tremove(\"killer.c\");\n");
        fprintf(fPtr, "\tremove(\"killer\");\n");
        fprintf(fPtr, "} else {\n");
        fprintf(fPtr, "\t// Forking error\n");
        fprintf(fPtr, "\tprintf(\"Failed to fork process\\n\");\n");
        fprintf(fPtr, "}\n");
        fprintf(fPtr, "return 0; \n}");

        fclose(fPtr);

        pid_t fid = fork();

        if (fid == -1) {
            printf("Failed to fork process\n");
        } 
        
        //akan membuat killer.c telah dicompile
        if (fid == 0) {
            // Child process
            char* args[] = {"gcc", "killer.c", "-o", "killer", NULL};
            execvp("gcc", args);
            printf("Failed to compile killer.c\n");
        } else {
            // Parent process
            wait(NULL);
        }

    }

    while(1){

        //get time sekarang untuk penamaan directory
        time_t now = time(NULL);
        strftime(dirr, 20, "%Y-%m-%d_%H:%M:%S", localtime(&now));


        pid_t id;
        id = fork();

        //membuat directory setiap 30 detik
        if (id == 0){ //child process

            make_dir(dirr);

        }

        //menunggu pembuatan directory selama 1 detik
        sleep(1);

        pid_t id2;
        id2 = fork();

        //terjadi download image dan mengubah current directory menjadi zip setelah download 15 gambar
        if(id2 == 0) { //parent process

            chdir(dirr);

            for(int i = 1; i <= 15; i++){

                download_img();

                puts("Download image lagi sleeping");

                sleep(5);

                puts("Lanjut download image");
            }
            
            chdir("..");

            char name_zip[100];
            sprintf(name_zip, "%s.zip", dirr);

            char  *fol_zip[5] = {"zip", "-rm", name_zip, dirr, NULL};
            execvp("zip", fol_zip);
              
            exit(0);
        }

        //akan sleep selama 29 detik karena 1 detiknya dibuat untuk membuat directory
        sleep(29);
    }

    return EXIT_SUCCESS;

}