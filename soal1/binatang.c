#define _GNU_SOURCE
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <dirent.h>
#include <string.h>

int main(int argc, char *argv[]) {
    pid_t pid;
    int status;
    char random_file_buf[512];

// fork 1 - download zip from link
    pid = fork();
    if (pid < 0) {
        perror("fork 1 failed");
        exit(1);
    }
    else if (pid == 0) {
        // child process
        execlp("wget", "wget", "-O", "binatang.zip", "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", NULL);
        perror("exec failed");
        exit(1);
    }
    else {
    // parent process
    wait(&status);
    if (WIFEXITED(status) && !WEXITSTATUS(status)) {
        printf("Download completed successfully\n");
// fork 2 - unzip the file 
        pid_t pid2 = fork();
        if (pid2 < 0) {
            perror("fork 2 failed");
            exit(1);
        } else if (pid2 == 0) {
            execlp("unzip", "unzip", "binatang.zip", "-d", "binatang_folder", NULL);
            perror("exec failed");
            exit(1);
        } else {
            wait(&status);
            if (WIFEXITED(status) && !WEXITSTATUS(status)) {
                system("rm -rf binatang.zip");
                printf("Unzip completed successfully\n");
// choose random file
                srand(time(NULL));
                DIR *dir = opendir("binatang_folder");
                struct dirent *entry;
                int file_count = 0;
                while ((entry = readdir(dir)) != NULL) {
                    if (entry->d_type == DT_REG) {
                        file_count++;
                        if (rand() % file_count == 0) {
                            sprintf(random_file_buf, "%s", entry->d_name);
                        }
                    }
                }
                closedir(dir);
                printf("\033[1;36m\nFile gambar acak adalah: %s\033[0m\n\n", random_file_buf);  //adding cyan color :)
// create directory HewanDarat, HewanAmphibi, HewanAir    
                int status;
                status = mkdir("HewanDarat", 0777);
                if (status != 0) {
                    perror("mkdir failed");
                    exit(1);
                }

                status = mkdir("HewanAmphibi", 0777);
                if (status != 0) {
                    perror("mkdir failed");
                    exit(1);
                }

                status = mkdir("HewanAir", 0777);
                if (status != 0) {
                    perror("mkdir failed");
                    exit(1);
                }
//filter file from binatang_folder
            //darat
                system("find . -name \"*_darat*\" -type f -exec sh -c 'for f; \
                        do if [ ! -e HewanDarat/\"$(basename \"$f\")\" ]; \
                        then mv \"$f\" HewanDarat/; \
                        fi; \
                        done' sh {} +");
            //amfibi
                system("find . -name \"*_amphibi*\" -type f -exec sh -c 'for f; \
                        do if [ ! -e HewanAmphibi/\"$(basename \"$f\")\" ]; \
                        then mv \"$f\" HewanAmphibi/; \
                        fi; \
                        done' sh {} +");
            // air
                system("find . -name \"*_air*\" -type f -exec sh -c 'for f; \
                do if [ ! -e HewanAir/\"$(basename \"$f\")\" ]; \
                then mv \"$f\" HewanAir/; \
                fi; \
                done' sh {} +");
            //remove binatang_folder
                system ("rm -rf binatang_folder");
//fork 3 - zip the folders
                char *type[]= {"HewanAir","HewanAmphibi", "HewanDarat"};
                char *zipName[] = {"HewanAir.zip","HewanAmphibi.zip", "HewanDarat.zip"};
                int size = sizeof(type) / sizeof(type[0]);
                pid_t pid3;
                for (int i=0; i<size; i++)
                {
                    pid3 = fork();
                    if (pid3 < 0) {
                        fprintf(stderr, "Fork 3 failed");
                    } else if (pid3 == 0) {
                        execlp("zip", "zip", "-r", zipName[i], type[i], NULL);
                        perror("exec failed");
                        exit(1);
                    } else {
                        wait(&status);
                        if (WIFEXITED(status) && !WEXITSTATUS(status)) {
                            printf("%s completed successfully\n", zipName[i]);
                            //remove the original folder before zipping
                            char cmd[100];
                            sprintf(cmd, "rm -rf %s", type[i]);
                            system(cmd);
                        }
                        else {
                            printf ("Zip failed\n");
                        }
                    }
                }

            } else {
                printf("Unzip failed\n");
            }
            }
    }
    else {
        printf("Download failed\n");
    }
}
}
